package com.components.pages;

import org.testng.Assert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.components.repository.SiteRepository;
import com.components.yaml.EnginesData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;

public class Hippo_EnginesPage extends SitePage 
{
	
	public Hippo_EnginesPage (SiteRepository repository)
	{
		super(repository);
	}
	
	public static final Target search_input_engines = new Target("search_input_engines","//*[@id=\"kp-global-search-input\"]",Target.XPATH);
	public static final Target search_btn_engines = new Target("search_btn_engines","//*[@id=\"search\"]",Target.XPATH);
	public static final Target search_header = new Target("search_results","//*[@id=\"kp-page-outer\"]/div/section/section/header/h1",Target.XPATH);
	public static final Target search_results_count = new Target("search_results_count","//*[@id=\"productResultsCount\"]/div", Target.XPATH);
	public static final Target search_results_span = new Target("search_results_span","//*[@id=\"productResultsCount\"]/div/span",Target.XPATH);
	public static final Target search_results_grid = new Target("search_results_grid","//*[@id=\"productResultsList\"]/div[2]",Target.XPATH);
	public static final Target dealer_find = new Target("dealer_find","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]/ul/li[6]/a/span",Target.XPATH);
	public static final Target dealer_header = new Target("dealer_header","//*[@id=\"kp-page-outer\"]/div/section/div[2]/section/header/h1",Target.XPATH);
	public static final Target dealer_zipCode = new Target("dealer_zipCode","//*[@id=\"dealerQuery\"]",Target.XPATH);
	public static final Target dealer_search_btn = new Target("dealer_search_btn","//*[@id=\"searchDiv\"]/div[4]/input",Target.XPATH);
	public static final Target dealer_search_results = new Target("dealer_search_results","//*[@id=\"resultsCount\"]/div/span",Target.XPATH);
	
	public static final Target contact_us_engines = new Target("contact_us_engines","//*[@id=\"kp-page-outer\"]/div/footer/ul/li[1]/div[2]/ul/li[1]/a",Target.XPATH);
	public static final Target contact_header = new Target("contact_header","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div/div/div/div[1]/h1",Target.XPATH);
	//Contact Us
	public static final Target contact_us_first_name = new Target("contact_us_first_name","//*[@id=\"page0\"]/fieldset[2]/div[1]/input",Target.XPATH);
	public static final Target contact_us_last_name = new Target("contact_us_last_name","//*[@id=\"page0\"]/fieldset[2]/div[2]/input",Target.XPATH);
	public static final Target contact_us_email = new Target("contact_us_email","//*[@id=\"page0\"]/fieldset[2]/div[3]/input",Target.XPATH);
	public static final Target contact_us_phone = new Target("contact_us_phone","//*[@id=\"page0\"]/fieldset[2]/div[4]/input",Target.XPATH);
	public static final Target contact_us_address = new Target("contact_us_address","//*[@id=\"page0\"]/fieldset[2]/div[5]/input",Target.XPATH);
	public static final Target contact_us_address2 = new Target("contact_us_address2","//*[@id=\"page0\"]/fieldset[2]/div[6]/input",Target.XPATH);
	public static final Target contact_us_city = new Target("contact_us_city","//*[@id=\"page0\"]/fieldset[2]/div[7]/input",Target.XPATH);
	public static final Target contact_us_postalcode = new Target("contact_us_postalcode","//*[@id=\"page0\"]/fieldset[2]/div[9]/input",Target.XPATH);
	public static final Target contact_us_company = new Target("contact_us_company","//*[@id=\"page0\"]/fieldset[2]/div[11]/input",Target.XPATH);
	public static final Target contact_us_model = new Target("contact_us_model","//*[@id=\"page0\"]/fieldset[3]/div[3]/input",Target.XPATH);
	public static final Target contact_us_comments = new Target("contact_us_comments","//*[@id=\"page0\"]/fieldset[4]/div/textarea",Target.XPATH);
	public static final Target contact_us_state = new Target("contact_us_state","//*[@id=\"page0\"]/fieldset[2]/div[8]/div",Target.XPATH);
	public static final Target contact_us_state_select = new Target("contact_us_state_select","//*[@id=\"page0\"]/fieldset[2]/div[8]/div/ul/li[54]",Target.XPATH);
	public static final Target contact_us_submit_btn = new Target("contact_us_submit_btn","//*[@id=\"kp-eform-491\"]/form/div[2]/div[2]/input",Target.XPATH);
	
	public static final Target homepage_global_banner = new Target("homepage_global_banner","//*[@id=\"gb-8a9t0w5t2-c--global-banner\"]",Target.XPATH);
	public static final Target homepage_global_nav = new Target("homepage_global_nav","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]",Target.XPATH);
	public static final Target homepage_hero_carousel = new Target("homepage_hero_carousel","//*[@id=\"primary-carousel\"]",Target.XPATH);
	public static final Target homepage_promo = new Target("homepage_promo","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[2]",Target.XPATH);
	public static final Target homepage_footer = new Target("homepage_footer","//*[@id=\"kp-page-outer\"]/div/footer/ul",Target.XPATH);
	
	public static final Target search_filter = new Target("search_filter","//*[@id=\"productResultsList\"]/div[1]/section/header",Target.XPATH);
	public static final Target search_sort = new Target("search_sort","//*[@id=\"productResultsSort\"]/div",Target.XPATH);
	public static final Target search_grid = new Target("search_results_grid","//*[@id=\"productResultsList\"]/div[2]",Target.XPATH);
	
	public static final Target pdp_breadcrumb = new Target("pdp_breadcrumb","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[1]",Target.XPATH);
	public static final Target pdp_maindisplayimage = new Target("pdp_maindisplayimage","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[1]/div[2]/div[1]/div/div/div[1]/div/div",Target.XPATH);
	public static final Target pdp_thumbnails = new Target("pdp_thumbnails","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[1]/div[2]/div[2]",Target.XPATH);
	public static final Target pdp_productinfo = new Target("pdp_productinfo","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[1]/div[1]",Target.XPATH);
	public static final Target pdp_finddealer = new Target("pdp_finddealer","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[1]/div[1]/div[5]/a[1]",Target.XPATH);
	public static final Target pdp_buyparts = new Target("pdp_buyparts","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[1]/div[1]/div[5]/a[2]",Target.XPATH);
	public static final Target pdp_tabs = new Target("pdp_tabs","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[2]/div",Target.XPATH);
	public static final Target pdp_techdocuments = new Target("pdp_techdocuments","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[1]/div[1]/div[4]",Target.XPATH);

public static final Target KohlerPowerWorldWide = new Target("KohlerPowerWorldWide","//*[@id='gb-8a9t0w5t2-c--click-to-toggle']",Target.XPATH);
	
	//Compare
	public static final Target Engines = new Target("Engines","//*[@id='kp-page-outer']/div/header/nav/div[2]/div/div[2]/ul/li[1]/a/span",Target.XPATH);
	public static final Target Diesel_Engines = new Target("Diesel_Engines","//h3[contains(text(),'Diesel Engines')]",Target.XPATH);		
	public static final Target Clear_All = new Target("Clear_All","//*[@id='kp-page-outer']/div/section/section/div/div/div[4]/div[2]/a[2]",Target.XPATH);	
	public static final Target Overlay = new Target("Overlay","//*[@id='kp-page-outer']/div/section/section/div/div/div[4]",Target.XPATH);
	
	EnginesData enginesData = EnginesData.fetch("EnginesData");
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	String browserName=caps.getBrowserName();
	
	public Hippo_EnginesPage atEnginesPage()
	{
		try 
		{
			if(caps.getBrowserName().equals("MicrosoftEdge"))
			{
				getCommand().driver.navigate().to("javascript:document.getElementById('overridelink').click()");
			}
			//Retrieving page state
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			String test = js.executeScript("return document.readyState").toString();
			//Verifying page load
			if (test.equalsIgnoreCase("complete")) {
				Assert.assertEquals(getCommand().driver.getTitle(), enginesData.page_Title);
				log ("Engines Page loaded",LogType.STEP);
			}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Engines search with search term returning no results
	public Hippo_EnginesPage verifyNoResults()
	{
		try
		{
			EnginesData enginesData = EnginesData.fetch("EnginesData");
			log("Search input: "+enginesData.search_NoResult+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_engines, enginesData.search_NoResult);
			getCommand().click(search_btn_engines);
			
			Assert.assertEquals(enginesData.search_Header, getCommand().getText(search_header).toUpperCase().trim(),"Search Results page did not open");
			log("Search Results page opened",LogType.STEP);
			Assert.assertEquals("0 results for \""+enginesData.search_NoResult+"\"", getCommand().getText(search_results_span),"0 results for: \"+enginesData.search_NoResult+\" not verified");
			log("0 results for: "+enginesData.search_NoResult+" verified",LogType.STEP);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Engines Search with Product search
	public Hippo_EnginesPage verifyProductResults()
	{
		try
		{
			EnginesData enginesData = EnginesData.fetch("EnginesData");
			log("Search input: "+enginesData.search_Product+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_engines, enginesData.search_Product);
			getCommand().click(search_btn_engines);
			getCommand().waitFor(10);
			boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/product/"+enginesData.search_Product.toLowerCase());
			Assert.assertEquals(status_PDP, true,"Product page not opened for: "+ enginesData.search_Product);
			log("Product page opened for: "+ enginesData.search_Product,LogType.STEP);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Find Dealer on Engines page
	public Hippo_EnginesPage verifyFindDealer()
	{
		try
		{
			getCommand().click(dealer_find);
			getCommand().waitFor(9);
			
			Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
			String browserName = caps.getBrowserName();

			if(browserName.equals("chrome"))
			{
				if(isAlertPresent())
				{
					Alert alert = getCommand().driver.switchTo().alert();
					alert.dismiss();
				}
			}
			
			
			Assert.assertEquals(getCommand().getText(dealer_header).toUpperCase().trim(), enginesData.dealer_Header,"Distributor Locator Page did not open");
			log("Distributor Locator Page opened",LogType.STEP);
			
			getCommand().clear(dealer_zipCode) .sendKeys(dealer_zipCode, enginesData.deal_ZipCode);
			getCommand().click(dealer_search_btn);
			log("Entered Zip Code and clicked on Search button",LogType.STEP);
			
			getCommand().waitFor(5);
			String search_results = getCommand().getText(dealer_search_results);
			log("Search Results: "+search_results,LogType.STEP);
			getCommand().clear(dealer_zipCode).sendKeys(dealer_zipCode, enginesData.deal_ZipCode1);
			getCommand().click(dealer_search_btn);
			log("Entered Another Zip Code and clicked on Search button",LogType.STEP);
			
			getCommand().waitFor(5);
			log("Search Results: "+getCommand().getText(dealer_search_results),LogType.STEP);
			Assert.assertNotEquals(getCommand().getText(dealer_search_results), search_results,"Results & Map not updated");
			log("Results & Map updated and verified",LogType.STEP);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Contact Us on Engines page
	public Hippo_EnginesPage verifyContactUs()
	{
		try
		{
			EnginesData enginesData = EnginesData.fetch("EnginesData");
			getCommand().scrollTo(contact_us_engines);
			log("Click on Contact under Contact Us",LogType.STEP);
			getCommand().sendKeys(contact_us_engines, Keys.chord(Keys.CONTROL,Keys.RETURN));
			
			getCommand().waitFor(4);
			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
			if(listofTabs.size()>1)
			{
				getCommand().driver.switchTo().window(listofTabs.get(1));
			}
			pageLoad();
			Assert.assertTrue(getCommand().getPageTitle().contains(enginesData.contact_Page_Title), enginesData.contact_Page_Title+" page did not open");
			log( getCommand().getText(contact_header)+" page Opened",LogType.STEP);
			
			getCommand().waitForTargetPresent(contact_us_state);
			//Filling Contact Us Form
			log("Filling the Contact Us form",LogType.STEP);
			getCommand().sendKeys(contact_us_first_name, enginesData.contact_us_firstname);
			getCommand().sendKeys(contact_us_last_name, enginesData.contact_us_lastname);
			getCommand().sendKeys(contact_us_email, enginesData.contact_us_email);
			getCommand().sendKeys(contact_us_phone, enginesData.contact_us_phone);
			getCommand().sendKeys(contact_us_address, enginesData.contact_us_address);
			getCommand().sendKeys(contact_us_address2, enginesData.contact_us_address2);
			getCommand().sendKeys(contact_us_city, enginesData.contact_us_city);
			getCommand().click(contact_us_state);
			getCommand().click(contact_us_state_select);
			getCommand().sendKeys(contact_us_postalcode, enginesData.contact_us_postalcode);
			getCommand().sendKeys(contact_us_company, enginesData.contact_us_company);
			getCommand().sendKeys(contact_us_model, enginesData.contact_us_model);
			getCommand().sendKeys(contact_us_comments, enginesData.contact_us_comments);
			
			//Captcha implementation not done
			log("Closing Contact Us page and switching to Main page",LogType.STEP);
			if(listofTabs.size()>1)
			{
				getCommand().driver.close();
				getCommand().driver.switchTo().window(listofTabs.get(0));
			}
			else
			{
				getCommand().driver.navigate().back();
			}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Footer links on Engines page
	public Hippo_EnginesPage VerifyFooter()
	{
		log("Verifying footer has 7 columns",LogType.STEP);
        // get the list of links available in footer
        WebElement footer_header = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/footer/ul"));
        List<WebElement> footer_headers = footer_header.findElements(By.className("menu-item__header"));
        WebElement footer_social = footer_header.findElement(By.className("menu-social__header"));
        footer_headers.add(footer_social);
        if(footer_headers.size() == 7)
        {
        	log("Footer is composed of 7 columns",LogType.STEP);
        	for(WebElement ele:footer_headers) 
        	{
        		log(ele.getText(),LogType.STEP);
        	}
        }
        
        else
        {
        	log("Footer does not have 7 columns",LogType.ERROR_MESSAGE);
        	Assert.fail("Footer does not have 7 columns");
        }     
        
        try 
        {
        	WebElement footer_count = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/footer/ul"));
            List<WebElement> header_sub_menu_count = footer_count.findElements(By.className("menu-item__sub-menu"));
            WebElement header_sub_menu_social_count = footer_count.findElement(By.className("menu-social__sub-menu"));
            header_sub_menu_count.add(header_sub_menu_social_count);
            List<WebElement> footer_links_count = new ArrayList<WebElement>();
        	
        	for(WebElement ele:header_sub_menu_count)
        	{
        		List<WebElement> header_sub_menu_links_count = ele.findElements(By.tagName("li"));
        		for(WebElement elem:header_sub_menu_links_count)
        		{
        			footer_links_count.add(elem);
        		}
        	}
        	
        	for(int count = 0; count < footer_links_count.size();count++)
        	{
        		WebElement footer = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/footer/ul"));
                List<WebElement> header_sub_menu = footer.findElements(By.className("menu-item__sub-menu"));
                WebElement header_sub_menu_social = footer.findElement(By.className("menu-social__sub-menu"));
                header_sub_menu.add(header_sub_menu_social);
                List<WebElement> footer_links = new ArrayList<WebElement>();
            	
            	for(WebElement ele:header_sub_menu)
            	{
            		List<WebElement> header_sub_menu_links = ele.findElements(By.tagName("li"));
            		for(WebElement elem:header_sub_menu_links)
            		{
            			footer_links.add(elem);
            		}
            	}
            	String elemText = footer_links.get(count).getText();
            	if (!elemText.equals(enginesData.notalink))
				{
            		if (!elemText.equals(enginesData.notalink1))
					{
						WebElement menu_link = footer_links.get(count).findElement(By.tagName("a"));
						String link_text = menu_link.getText();
						String pageTitle = getCommand().getPageTitle();
						if (!link_text.contains("Find a Dealer"))
						{
							getCommand().scrollTo(contact_us_engines);
    						log("Clicking on the link: "+menu_link.getText(),LogType.STEP);
    						if(browserName.equals("safari"))
    						{
    							menu_link.sendKeys(Keys.chord(Keys.COMMAND,Keys.RETURN));
    						}
    						else
    						{
    							menu_link.sendKeys(Keys.chord(Keys.CONTROL,Keys.RETURN));
    						}
 		
    						getCommand().waitFor(4);
    						ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());     						
    						
    						log("Switching to "+elemText+" page",LogType.STEP);
    						if(listofTabs.size()>1) 							
    						{
    							if(browserName.equals("safari"))
    							{
    								getCommand().driver.switchTo().window(listofTabs.get(0));
    							}
    							else
    							{
    							getCommand().driver.switchTo().window(listofTabs.get(1));
    							}
    						}
    						getCommand().waitFor(2);
    						pageLoad();
                        	Assert.assertNotEquals(pageTitle, getCommand().getPageTitle(),"Page did not open");
                        	log("Switching back to main page",LogType.STEP);
                        	if(listofTabs.size()>1)
                        	{
                        		getCommand().driver.close();
                        		if(browserName.equals("safari"))
                        		{
                        			getCommand().driver.switchTo().window(listofTabs.get(1));
                        		}
                        		else
                        		{
                        		getCommand().driver.switchTo().window(listofTabs.get(0));
                        		}
                        	}
                        	else
                        	{
                        		getCommand().driver.navigate().back();
                        	}
						}
					}
            		else 
            		{
            			log(elemText+" is not a hyperlink",LogType.ERROR_MESSAGE);
            		}
				}
            	else
            	{
            		log(elemText+" is not a hyperlink",LogType.ERROR_MESSAGE);
            	}
        	}
        	
        	getCommand().click(dealer_find);
			getCommand().waitFor(5);
			Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
			if(caps.getBrowserName().equals("chrome"))
			{
				if(isAlertPresent())
				{
					Alert alert = getCommand().driver.switchTo().alert();
					alert.dismiss();
				}
			}
			getCommand().waitFor(5);
			Assert.assertEquals(enginesData.dealer_Header,getCommand().getText(dealer_header).toUpperCase().trim(), "Find a Dealer Page did not open");
			log("Find a Dealer Page opened",LogType.STEP);
        }
	catch(Exception ex)
        {
			Assert.fail(ex.getMessage());
        }
		return this;
	}
	
	//Verify Engines Home page
	public Hippo_EnginesPage VerifyHomePage()
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gb-8a9t0w5t2-c--click-to-toggle\"]")));
			//Global Banner
			getCommand().waitForTargetVisible(homepage_global_banner);
			WebElement banner_text = getCommand().driver.findElement(By.xpath("//*[@id=\"gb-8a9t0w5t2-c--click-to-toggle\"]"));
			if (getCommand().isTargetVisible(homepage_global_banner) && banner_text.getText().trim().equals(enginesData.banner_text))
			{
				log("Global Banner "+banner_text.getText()+" is displayed",LogType.STEP);
			}
			else
			{
				log(enginesData.banner_text+" is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail(enginesData.banner_text+" is not displayed");
			}
			
			//Kohler Logo
			if (getCommand().isTargetVisible(homepage_global_banner) && banner_text.getText().trim().equals(enginesData.banner_text))
			{
				log("Kohler Logo is displayed",LogType.STEP);
			}
			else
			{
				log("Kohler Logo is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Kohler Logo is not displayed");
			}
			
			//Utility Navigation
			WebElement utility_nav = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]"));
			List<WebElement> utility_menu_links = utility_nav.findElements(By.tagName("li"));
			log("Utility Navigation bar consisting of below is displayed:",LogType.STEP);
			for (WebElement ele:utility_menu_links)
			{
				WebElement display_text = ele.findElement(By.tagName("a"));
				if (utility_nav.isDisplayed() && ele.isDisplayed())
				{
					log(display_text.getText(), LogType.SUBSTEP);
				}
			}
			
			//Search
			WebElement search_box = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[3]"));
			if (search_box.isDisplayed())
			{
				log("Search Box is displayed",LogType.STEP);
			}
			else
			{
				log("Search Box is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Box is not displayed");
			}
			
			//Carousel
			if (getCommand().isTargetVisible(homepage_hero_carousel))
			{
				log("Hero Carousel is displayed",LogType.STEP);
			}
			else
			{
				log("Hero Carousel is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Carousel is not displayed");
			}
			
			//Promo		
			if(getCommand().isTargetVisible(homepage_promo))
			{
				log("Promo modules are displayed",LogType.STEP);
			}
			else
			{
				log("Promo modules are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Promo modules are not displayed");
			}
			
			//Footer
			if(getCommand().isTargetVisible(homepage_footer))
			{
				log("Footer is displayed",LogType.STEP);
			}
			else
			{
				log("Footer is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Footer is not displayed");
			}
		}
		catch( Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Engines Search Page
	public Hippo_EnginesPage verifySearchPage()
	{
		try
		{
			EnginesData enginesData = EnginesData.fetch("EnginesData");
			log("Search input: "+enginesData.search_Result+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_engines, enginesData.search_Result);
			getCommand().click(search_btn_engines);
			
			Assert.assertEquals(enginesData.search_Header, getCommand().getText(search_header).toUpperCase().trim(), "Search Results page did not open");
			log("Search Results page opened",LogType.STEP);
			
			getCommand().waitFor(8);
			
			WebElement tablist = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/section/div[2]/div[1]/ul"));
			
			//Search Tabs
			if (tablist.isDisplayed())
			{
				log("Search Results tab contains following tabs:",LogType.STEP);
				List<WebElement> tabs = tablist.findElements(By.tagName("li"));
				tabs.get(0).click();
				for(WebElement ele:tabs)
				{
					log(ele.getText(),LogType.SUBSTEP);
				}
			}
			else
			{
				log("Search Results tab not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Results tab not displayed");
			}
			
			//Search Filters & Categories
			if (getCommand().isTargetVisible(search_filter))
			{
				log("Search Filter is present and has following categories:",LogType.STEP);
				WebElement categories = getCommand().driver.findElement(By.xpath("//*[@id=\"productResultsList\"]/div[1]/section/div"));
				List<WebElement> categories_list = categories.findElements(By.className("facet-group-header"));
				
				for (WebElement ele:categories_list)
				{
					log(ele.getText(),LogType.SUBSTEP);
				}
			}
			else
			{
				log("Search Filters not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Filters not displayed");
			}
			
			//Sort by
			if (getCommand().isTargetVisible(search_sort))
			{
				log("Sort by dropdown is present",LogType.STEP);
			}
			else
			{
				log("Sort by dropdown not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Sort by dropdown not displayed");
			}
			
			//Results Grid
			if(getCommand().isTargetVisible(search_grid))
			{
				log("Search Results grid is displayed",LogType.STEP);
			}
			else
			{
				log("Search Results grid not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Results grid not displayed");
			}			
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Engines PDP page
	public Hippo_EnginesPage verifyPDP()
	{
		try
		{
			
			EnginesData enginesData = EnginesData.fetch("EnginesData");
			log("Search input: "+enginesData.search_Product+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_engines, enginesData.search_Product);
			getCommand().click(search_btn_engines);
			getCommand().waitFor(10);
			boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/product/"+enginesData.search_Product.toLowerCase());
			Assert.assertEquals(status_PDP, true, "Product page for: "+ enginesData.search_Product +" did not open");
			log("Product page opened for: "+ enginesData.search_Product,LogType.STEP);
			
			//Breadcrumb
			getCommand().waitForTargetVisible(pdp_breadcrumb);
			if(getCommand().isTargetVisible(pdp_breadcrumb))
			{
				log("Breadcrumb is displayed",LogType.STEP);
			}
			else
			{
				log("Breadcrumb is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Breadcrumb is not displayed");
			}
			
			//Main display image 
			getCommand().waitForTargetVisible(pdp_maindisplayimage);
			if(getCommand().isTargetVisible(pdp_maindisplayimage))
			{
				log("Main display image is displayed",LogType.STEP);
			}
			else
			{
				log("Main display image is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Main display image is not displayed");
			}
			
			//Thumbnails 
			if(getCommand().isTargetVisible(pdp_thumbnails))
			{
				log("Thumbnails are displayed",LogType.STEP);
			}
			else
			{
				log("Thumbnails are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Thumbnails are not displayed");
			}
			
			//Product Info 
			if(getCommand().isTargetVisible(pdp_productinfo) && getCommand().getText(pdp_productinfo).contains(enginesData.search_Product))
			{
				log("Model# & Product Info is displayed:",LogType.STEP);
				log(getCommand().getText(pdp_productinfo),LogType.SUBSTEP);
			}
			else
			{
				log("Model# & Product Info is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Model# & Product Info is not displayed");
			}
			
			//Find Dealer & Buy Parts
			if(getCommand().isTargetVisible(pdp_finddealer) && getCommand().isTargetVisible(pdp_buyparts))
			{
				log("Find Dealer & Buy Parts are displayed",LogType.STEP);
			}
			else
			{
				log("Find Dealer & Buy Parts are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Find Dealer & Buy Parts are not displayed");
			}
			
			//Specs, Engine Uses & Service
			List<String> productTabs = new ArrayList<String>();
			List<String> actualProductTabs = new ArrayList<String>();
			productTabs.add(enginesData.pdp_Tab1);
			productTabs.add(enginesData.pdp_Tab2);
			productTabs.add(enginesData.pdp_Tab3);
			
			if(getCommand().isTargetVisible(pdp_tabs))
			{
				WebElement pdpTabs = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[2]/div[2]/div/ul"));
				List<WebElement> tabs = pdpTabs.findElements(By.tagName("li"));
				log("Below tabs are displayed:",LogType.STEP);
				for (WebElement ele:tabs)
				{
					actualProductTabs.add(ele.getText().trim());
					log(ele.getText(),LogType.SUBSTEP);
				}
				CompareTabs (productTabs, actualProductTabs);
			}
			else
			{
				log("Specs, Engine Uses & Service tabs are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Specs, Engine Uses & Service tabs are not displayed");
			}
			
			//Technical Documents 
			if(getCommand().isTargetVisible(pdp_techdocuments))
			{
				log("Technical Documents are displayed",LogType.STEP);
			}
			else
			{
				log("Technical Documents are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Technical Documents are not displayed");
			}
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Hippo_EnginesPage VerifyCategorypageforEngines() 
	{
		try
		{
			getCommand().driver.findElement(By.xpath("(//li[@class='nav-links__link'])[1]/a")).click();
			getCommand().waitFor(5);
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			js.executeScript("window.scrollBy(0,500)");
			getCommand().waitFor(5); 
			getCommand().driver.findElement(By.xpath("//a[contains(text(),'View All Engines')]")).click();
			log("Verify Category Page Layout",LogType.STEP);
			getCommand().waitFor(5);

			WebElement categorypage=getCommand().driver.findElement(By.xpath("//div[@ng-app='kpProductSearch']/div/div[1]/h2"));

			if(categorypage.isDisplayed())
			{
				log("category page is displayed", LogType.STEP);
			}
			else
			{
				log("category page is not displayed", LogType.ERROR_MESSAGE);
				Assert.fail("category page is not displayed");
			}

			List<WebElement> Breadcrumbbar=getCommand().driver.findElements(By.xpath("//div[@class='breadcrumbs']/ul/li"));

			for(WebElement Breadcrumb : Breadcrumbbar)
			{
				if(Breadcrumb.isDisplayed())
				{
					log("Breadcrumb is displayed", LogType.STEP);
				}
				else
				{
					log("Breadcrumb is not displayed", LogType.ERROR_MESSAGE);
					Assert.fail("Breadcrumb is not displayed");
				}
			}

			int X=getCommand().driver.manage().window().getSize().getWidth();

			WebElement Filters=getCommand().driver.findElement(By.xpath("//header[@ng-model='facetsActive']/following-sibling::div"));

			int x=Filters.getLocation().getX();

			if(Filters.isDisplayed() && x<(X/2))
			{
				log("ThumbNails are displayed to the left of main image",LogType.STEP);
			}
			else
			{
				log("ThumbNails are not displayed to the left of main image",LogType.ERROR_MESSAGE);
				Assert.fail("ThumbNails are not displayed to the left of main image");
			}

			List<WebElement> SortBy=getCommand().driver.findElements(By.xpath("//div[@ng-if='hasresults']/div[2]/ul/li"));

			if(SortBy.size()!=0)
			{
				log("Sort by drop-down is present", LogType.STEP);
			}
			else
			{
				log("Sort by drop-down is not present", LogType.ERROR_MESSAGE);
				Assert.fail("Sort by drop-down is not present");
			}

			List<WebElement> ProductGrid=getCommand().driver.findElements(By.xpath("//section[contains(@class,'result-item ')]"));

			if(ProductGrid.size()!=0)
			{
				log("ProductGrid is present", LogType.STEP);
			}
			else
			{
				log("ProductGrid is not present", LogType.ERROR_MESSAGE);
				Assert.fail("ProductGrid is not present");
			}
		}

		catch(Exception e)
		{
			Assert.fail(e.getMessage());
		}
        return this;
	}
	
	public Hippo_EnginesPage VerifyProductSliderforEngines()
    {
		try
		{
			log("Verifying Engines Page",LogType.STEP);
			getCommand().driver.findElement(By.xpath("(//li[@class='nav-links__link'])[1]/a")).click();
			getCommand().waitFor(2);
			
			if(getCommand().getPageTitle().contains("Engines"))
				Assert.assertTrue(true, "Successfully in Engines Page");
                            
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			js.executeScript("window.scrollBy(0,500)");
			getCommand().waitFor(2); 
			getCommand().driver.findElement(By.xpath("//a[contains(text(),'View All Engines')]")).click();
			getCommand().waitFor(5);
			if(getCommand().getPageUrl().contains("products"))
				Assert.assertTrue(true, "Successfully in Engine products Page");
			getCommand().waitFor(2);

			String ProductResults = getCommand().driver.findElement(By.xpath("//*[@class=\"product-search__results__sort__count ng-binding\"]")).getText().trim();
                        
			getCommand().waitFor(2);
			log("Verifying that filter is visble in generators Page ",LogType.STEP);

			getCommand().driver.findElement(By.xpath("(//span[contains(text(),'Gasoline')])[1]//preceding-sibling::input")).click();
		
			getCommand().waitFor(5);
			String gasolineResults = getCommand().driver.findElement(By.xpath("//*[@class=\"product-search__results__sort__count ng-binding\"]")).getText();
			
			if((gasolineResults)!=(ProductResults))
			{
				log("product grid updated accordingly using fuel type filters",LogType.STEP);
			}
			else
			{
				log("product grid is not updated using fuel type filters",LogType.ERROR_MESSAGE);
				Assert.fail("product grid is not updated fuel type filters");
			}
			log("Verifying that slider is visble in generators Page ",LogType.STEP);

			WebElement KW_lefthand = getCommand().driver.findElement(By.xpath("(//span[@class='rz-bar-wrapper'])[3]//following-sibling::span[2]"));
			Actions action = new Actions(getCommand().driver);
                        
			log("drag the slider to certain range",LogType.STEP);
			
			WebElement slider = getCommand().driver.findElement(By.xpath("//*[@class=\"rzslider ng-isolate-scope\"]"));

						
		  action.moveToElement(slider).click(KW_lefthand).sendKeys(Keys.ARROW_RIGHT).build().perform();
			 
			getCommand().waitFor(5);
			
			String KWfilterResults = getCommand().driver.findElement(By.xpath("//*[@class=\"product-search__results__sort__count ng-binding\"]")).getText().trim();
			System.out.print(KWfilterResults);
			if((KWfilterResults)!=(ProductResults))
			{
				log("product grid updated accordingly using KW range slider function",LogType.STEP);
				log(KW_lefthand.getCssValue("left"), LogType.STEP);
			}
			else
			{
				log("product grid is not updated using KW range slider function",LogType.ERROR_MESSAGE);
				Assert.fail("product grid is not updated using KW range slider function");
			}    
			getCommand().waitFor(2);
             
			
			
           	}
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
			ex.printStackTrace();
		}	           
		return this;
    }
	
	public Hippo_EnginesPage  VerifyHippoEngine_HeroImage() throws InterruptedException
    {
		try
		{
			String HeroImage1 = "//div[@id='slick-slide00']/div/div/div[1]";
			String HeroImage2 = "//div[@id='slick-slide01']/div/div/div[1]";
			String HeroImage3 = "//div[@id='slick-slide02']/div/div/div[1]";
			String HeroImage4 = "//div[@id='slick-slide03']/div/div/div[1]";
			String HeroImage5 = "//div[@id='slick-slide04']/div/div/div[1]";
			
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			log("Clicking on Navigation dot2 to get second hero image",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control01']")).click();
			getCommand().waitFor(10);
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			if(!(HeroImage2==HeroImage1))
			{
				log("Hero Image changed After click on Navigation dots",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}
			
			log("Clicking on Navigation dot3 to get third hero image",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control02']")).click();
			getCommand().waitFor(10);
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			if(!(HeroImage3==HeroImage2))
			{
				log("Hero Image changed After click on Navigation dots",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}
			
			log("Clicking on Navigation dot4 to get fourth hero image",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control03']")).click();
			getCommand().waitFor(10);
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			if(!(HeroImage4==HeroImage3))
			{
				log("Hero Image changed After click on Navigation dots",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}
			
			log("Clicking on Navigation dot5 to get fifth hero image",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control04']")).click();
			getCommand().waitFor(10);
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			if(!(HeroImage5==HeroImage4))
			{
				log("Hero Image changed After click on Navigation dots",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}
			
			log("Clicking on Navigation dot1 to get first hero image",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control00']")).click();
			getCommand().waitFor(10);
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			if(!(HeroImage1==HeroImage5))
			{
				log("Hero Image changed After click on Navigation dots",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}
			
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			if(!(HeroImage1==HeroImage5))
			{
				log("Hero Image changed After clicking on previous icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}              
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			if(!(HeroImage1==HeroImage2))
			{
				log("Hero Image changed After clicking on Next icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}
			
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			if(!(HeroImage1==HeroImage2))
			{
				log("Hero Image changes After clicking on previous icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}              
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			if(!(HeroImage2==HeroImage3))
			{
				log("Hero Image changed After clicking on Next icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}
			
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			if(!(HeroImage2==HeroImage3))
			{
				log("Hero Image changes After clicking on previous icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}              
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			if(!(HeroImage3==HeroImage4))
			{
				log("Hero Image changed After clicking on Next icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}
			
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			if(!(HeroImage3==HeroImage4))
			{
				log("Hero Image changed After clicking on previous icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}              
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			if(!(HeroImage4==HeroImage5))
			{
				log("Hero Image changed After clicking on Next icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}
			
			////////
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			if(!(HeroImage4==HeroImage5))
			{
				log("Hero Image changed After clicking on previous icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}              
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			if(!(HeroImage5==HeroImage1))
			{
				log("Hero Image changed After clicking on Next icon",LogType.STEP);
			}
			else
			{
				log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}
			
		}
		catch(Exception e)
		{
			Assert.fail(e.getMessage());
		}
		return this;
    }
	
	public Hippo_EnginesPage Engines_WorldWide() throws InterruptedException
    {
		try 
		{
			List<String> WorldWideBannerRegionsList = new ArrayList<String>();                  
			int  WorldWideBanner_Columnscount = 0;
            log("Checking worldwide banner for Engines Site",LogType.STEP);
            log("Clicking on KOHLER Engines Banner",LogType.STEP);

            getCommand().isTargetPresent(KohlerPowerWorldWide);                  
            getCommand().click(KohlerPowerWorldWide);
            log("Verifying WorldWide Banner is composed of 7 columns",LogType.STEP);
            
		    List<WebElement> WorldWideBanner = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/ul"));
		    for(WebElement WorldWidebanner : WorldWideBanner)
		    {
		    	if(WorldWidebanner.isDisplayed())
		    	{
		    		WorldWideBanner_Columnscount++;
		    	}
		    }  
		    if(WorldWideBanner_Columnscount == 7)
		    {
		    	log("WorldWide Banner is displayed with 7 columns",LogType.STEP);             
		        log("Checking WorldWide Banner is displayed with 7 different regions",LogType.STEP);
		                 
		        List<WebElement> WorldWideBannerRegions = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/h3"));
		        log("Getting regions text and verifying all are of different regions",LogType.STEP);
		                 
		        for (WebElement WorldWideBannerRegion : WorldWideBannerRegions)
		        {
		        	WorldWideBannerRegionsList.add(WorldWideBannerRegion.getText());
		        }		        
		        
		        Assert.assertTrue(CompareDataFromSameList(WorldWideBannerRegionsList),"WorldWide Banner is not displayed with 7 different regions");         
		        log("WorldWide Banner is displayed with 7 different regions",LogType.STEP);                 
		                                 
		        List<WebElement> AllRegionLinksCount = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/ul/li/a"));		                 
		        String Pageurl = getCommand().getPageUrl();
		                 
		        log("Clicking on various links and verifying they are Navigated as expected",LogType.STEP);
		                 
		        for(int count = 0; count < AllRegionLinksCount.size(); count++)
				{	
		        	List<WebElement> AllRegionLinks = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/ul/li/a"));
		        	String Linktext = AllRegionLinks.get(count).getText();
		            log("Clicking and opening the link "+Linktext+ " in new tab",LogType.STEP); 
		            
                    if(browserName.equals("safari"))
                    {
                        String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN);
                        AllRegionLinks.get(count).sendKeys(selectLinkOpeninNewTab);

                    }
                    else
                    {
                        String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
                        AllRegionLinks.get(count).sendKeys(selectLinkOpeninNewTab);

                    }
		            
		            getCommand().waitFor(5);
		
		            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
		            log("Switching to new tab",LogType.STEP);
		            
		            if(listofTabs.size() > 1)
		            {
		            	if(browserName.equals("safari"))
                    	{
                        	getCommand().driver.switchTo().window(listofTabs.get(0));

                    	}
                    	else
                    	{
                        	getCommand().driver.switchTo().window(listofTabs.get(1));

                    	}
		            }
		            pageLoad();        
		            log("Getting new tab page title",LogType.STEP);
		            String CurrentpageUrl = getCommand().getPageUrl();
		                    
		            if(Pageurl.equals(CurrentpageUrl))
		            {
		            	log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
		                Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
		            }
		            else
		            {
		            	log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
		            }
		            
		            if(listofTabs.size() > 1)
		            {
		            	getCommand().driver.close();
                    	if(browserName.equals("safari"))
                    	{
                        	getCommand().driver.switchTo().window(listofTabs.get(1));

                    	}
                    	else
                    	{
                        	getCommand().driver.switchTo().window(listofTabs.get(0));

                    	} 
		            }
		            else
		            {
		            	getCommand().driver.navigate().back();
		            	pageLoad();
		            }
		        }
		    }
		}
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
    }
	  
	public Hippo_EnginesPage VerifyCompareFeature() throws InterruptedException
    {
		try
        {
			log("Click on Engines link",LogType.STEP);
			getCommand().driver.findElement(By.xpath("(//li[@class='nav-links__link'])[1]/a")).click();
			getCommand().waitFor(5);
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
            js.executeScript("window.scrollBy(0,500)");
			getCommand().waitFor(5); 
			log("Click on View all engines link",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//a[contains(text(),'View All Engines')]")).click();
			log("Verify Category Page Layout",LogType.STEP);
			getCommand().waitFor(5);        
			String Parent=getCommand().driver.getWindowHandle();
			log("Listing all the products present in the page",LogType.STEP);
			List<WebElement> products=getCommand().driver.findElements(By.xpath("//section[contains(@class,'result-item')]"));
			if(caps.getBrowserName().equals("MicrosoftEdge") || caps.getBrowserName().equals("safari") )
            {
            	js.executeScript("window.scrollBy(0,200)");
            }
            else
            {
            	js.executeScript("window.scrollBy(0,400)");
            }
			getCommand().waitFor(2); 
			for(int i=0;i<3;i++)
			{
				getCommand().driver.switchTo().window(Parent);
				WebElement element=products.get(i);
				Actions sact=new Actions(getCommand().driver);
				sact.moveToElement(element).build().perform();
				log("Click on the compare link in the product",LogType.STEP);
				List<WebElement> Compare=getCommand().driver.findElements(By.xpath("//label[@class='form-element__compare']"));
				Compare.get(i).click();
				Set<String> s1=getCommand().driver.getWindowHandles();
				Iterator<String> it=s1.iterator();
				while(it.hasNext())
				{
					String ChildWindow=it.next();
					getCommand().driver.switchTo().window(ChildWindow);
					log("switching to compare products window",LogType.STEP);
				}
				getCommand().waitFor(5);
			}

			List<WebElement> CompareCount =getCommand().driver.findElements(By.xpath("//div[@class='product-compare__products']/div"));
			if(CompareCount.size()==4)
			{
				for(int l=1;l<=3;l++)
				{
					log("Verify compare panel shows three product modules (with thumbnail, product name, SKU# and price) each with a close icon",LogType.STEP);
					getCommand().driver.findElement(By.xpath("(//div[@class='product-compare__image'])["+l+"]")).isDisplayed();
					getCommand().driver.findElement(By.xpath("(//div[@class='product-compare__image'])["+l+"]/following-sibling::h3")).isDisplayed();
					getCommand().driver.findElement(By.xpath("(//button[@class='closer'])["+l+"]")).isDisplayed();
				}
				log("Verify clear results link, compare CTA and expand arrow is displayed",LogType.STEP);
				getCommand().driver.findElement(By.xpath("(//a[contains(text(),'Clear all')])")).isDisplayed();
				getCommand().driver.findElement(By.xpath("//a[contains(text(),'Compare')]")).isDisplayed();
			} 
			else
			{	
				log("Compare panel does not displays product modules",LogType.ERROR_MESSAGE);
				Assert.fail("Compare panel does not displays product modules");
			}
			log("Click on Compare CTA link",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//a[contains(text(),'Compare')]")).click();
			getCommand().waitFor(5);
        
			for(int k=2;k<=4;k++)
			{
				log("Verify Each product column has an image, product name, SKU# and price, and product features",LogType.STEP);
				boolean images=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[2]/div["+k+"]")).isDisplayed();
				boolean sku=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[1]/div["+k+"]")).isDisplayed();
				boolean productName=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[3]/div["+k+"]")).isDisplayed();
				boolean primeRange=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[4]/div["+k+"]")).isDisplayed();
				boolean continousRange=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[5]/div["+k+"]")).isDisplayed();
				boolean fueltype=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[6]/div["+k+"]")).isDisplayed();
				boolean frequency=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[7]/div["+k+"]")).isDisplayed();
				boolean speed=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[8]/div["+k+"]")).isDisplayed();
				boolean alternateType=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[9]/div["+k+"]")).isDisplayed();
				boolean engineManufacturer=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[10]/div["+k+"]")).isDisplayed();
				boolean emissons=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[11]/div["+k+"]")).isDisplayed();
				
				if(images && sku && productName  && primeRange && continousRange && fueltype && frequency && speed && alternateType && engineManufacturer && emissons ==true)
				{	
					log("Each product column has an image, product name, SKU# and product features",LogType.STEP);
				}
				else
				{
					log("Missing elements from product features",LogType.ERROR_MESSAGE);
					Assert.fail("Missing elements from product features");
				}     
			}  
        }
		catch(Exception e)
        {
			Assert.fail(e.getMessage());
        }
		return this;
    }
	
	//Helpers
	//Compare Data from Lists
	public boolean CompareDataFromSameList(List<String> list)
	{
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(list.get(i).equals(list.get(k)))
				{
					Assert.fail("Mismatch in data present in the list");
				}				      
			}	      
		}		
		return this != null;
	}
	
	//Compare Tabs
	public Hippo_EnginesPage CompareTabs(List<String> expectedList, List<String> actualList)
	{
		if (expectedList.size() == actualList.size())
		{
			for (int i=0; i < actualList.size(); i++)
			{
				if (!expectedList.get(i).equalsIgnoreCase(actualList.get(i)))
				{
					Assert.fail("Mismatch in product Tabs");
				}
			}
		}	
		return this;
	}
	
	//Alert Handling
	public boolean isAlertPresent()
	{
		try
		{
			getCommand().driver.switchTo().alert();
			return true;
		}
		catch(NoAlertPresentException ex)
		{
			return false;
		}
	}
	
	//PageLoad
	public Hippo_EnginesPage pageLoad()
	{
		try
		{
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			String test = js.executeScript("return document.readyState").toString();
			
			while (!test.equalsIgnoreCase("complete"))
			{
				getCommand().waitFor(1);
				test = js.executeScript("return document.readyState").toString();
			}
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
};