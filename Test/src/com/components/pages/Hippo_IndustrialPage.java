package com.components.pages;

import org.testng.Assert;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.components.repository.SiteRepository;
import com.components.yaml.IndustrialData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Hippo_IndustrialPage extends SitePage {
	
	public Hippo_IndustrialPage (SiteRepository repository)
	{
		super(repository);
	}
	
	public static final Target search_input_industrial = new Target("search_input_industrial","//*[@id=\"kp-global-search-input\"]",Target.XPATH);
	public static final Target search_btn_industrial = new Target("search_btn_industrial","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[3]/form/div/a/div",Target.XPATH);
	public static final Target search_header = new Target("search_results","//*[@id=\"kp-page-outer\"]/div/section/section/header/h1",Target.XPATH);
	public static final Target search_results_count = new Target("search_results_count","//*[@id=\"productResultsCount\"]/div", Target.XPATH);
	public static final Target search_results_span = new Target("search_results_span","//*[@id=\"productResultsCount\"]/div/span",Target.XPATH);
	public static final Target search_results_grid = new Target("search_results_grid","//*[@id=\"productResultsList\"]/div[2]",Target.XPATH);
	public static final Target distributor_locator = new Target("distributor_locator","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]/ul/li[6]/a/span",Target.XPATH);
	public static final Target distributor_header = new Target("distributor_header","//*[@id=\"kp-page-outer\"]/div/section/div[2]/section/header/h1",Target.XPATH);
	public static final Target distributor_zipCode = new Target("distributor_zipCode","//*[@id=\"dealerQuery\"]",Target.XPATH);
	public static final Target distributor_search_btn = new Target("distributor_search_btn","//*[@id=\"searchDiv\"]/input[2]",Target.XPATH);
	public static final Target distributor_search_results = new Target("distributor_search_results","//*[@id=\"resultsCount\"]/div/span",Target.XPATH);
	public static final Target contact_us_industrial = new Target("contact_us_industrial","//*[@id=\"kp-page-outer\"]/div/footer/ul/li[1]/div[1]/h2",Target.XPATH);
	public static final Target send_message_industrial = new Target("send_message_industrial","//*[@id=\"kp-page-outer\"]/div/footer/ul/li[1]/div[2]/ul/li[1]/a",Target.XPATH);
	public static final Target find_distributor_industrial = new Target("find_distributor_industrial","//*[@id=\"kp-page-outer\"]/div/footer/ul/li[1]/div[2]/ul/li[3]/a", Target.XPATH);
	public static final Target e_newsletter_industrial = new Target("e_newsletter_industrial","//*[@id=\"kp-page-outer\"]/div/footer/ul/li[1]/div[2]/ul/li[4]/a",Target.XPATH);

	//Contact Us
	public static final Target contact_us_first_name = new Target("contact_us_first_name","//*[@id=\"page0\"]/fieldset[2]/div[1]/input",Target.XPATH);
	public static final Target contact_us_last_name = new Target("contact_us_last_name","//*[@id=\"page0\"]/fieldset[2]/div[2]/input",Target.XPATH);
	public static final Target contact_us_email = new Target("contact_us_email","//*[@id=\"page0\"]/fieldset[2]/div[3]/input",Target.XPATH);
	public static final Target contact_us_phone = new Target("contact_us_phone","//*[@id=\"page0\"]/fieldset[2]/div[4]/input",Target.XPATH);
	public static final Target contact_us_address = new Target("contact_us_address","//*[@id=\"page0\"]/fieldset[2]/div[5]/input",Target.XPATH);
	public static final Target contact_us_address2 = new Target("contact_us_address2","//*[@id=\"page0\"]/fieldset[2]/div[6]/input",Target.XPATH);
	public static final Target contact_us_city = new Target("contact_us_city","//*[@id=\"page0\"]/fieldset[2]/div[7]/input",Target.XPATH);
	public static final Target contact_us_postalcode = new Target("contact_us_postalcode","//*[@id=\"page0\"]/fieldset[2]/div[9]/input",Target.XPATH);
	public static final Target contact_us_company = new Target("contact_us_company","//*[@id=\"page0\"]/fieldset[2]/div[11]/input",Target.XPATH);
	public static final Target contact_us_comments = new Target("contact_us_comments","//*[@id=\"page0\"]/fieldset[4]/div/textarea",Target.XPATH);
	public static final Target contact_us_state = new Target("contact_us_state","//*[@id=\"page0\"]/fieldset[2]/div[8]/div",Target.XPATH);
	public static final Target contact_us_state_select = new Target("contact_us_state_select","//*[@id=\"page0\"]/fieldset[2]/div[8]/div/ul/li[49]",Target.XPATH);
	public static final Target contact_us_describe = new Target("contact_us_describe","//*[@id=\"page0\"]/fieldset[2]/div[12]/div",Target.XPATH);
	public static final Target contact_us_describe_select = new Target("contact_us_describe_select","//*[@id=\"page0\"]/fieldset[2]/div[12]/div/ul/li[13]",Target.XPATH);
	public static final Target contact_us_submit_btn = new Target("contact_us_submit_btn","//*[@id=\"kp-eform-102\"]/form/div[2]/div[2]/input",Target.XPATH);
	
	public static final Target contact_us_find_dealer = new Target("contact_us_find_dealer","//*[@id=\"kp-page-outer\"]/div/footer/ul/li[1]/div[2]/ul/li[3]/a",Target.XPATH);
	public static final Target contact_us_newsletter = new Target("contact_us_newsletter","//*[@id=\"kp-page-outer\"]/div/footer/ul/li[1]/div[2]/ul/li[4]/a",Target.XPATH);
	public static final Target contact_us_newsletter_header = new Target("contact_us_newsletter_header","/html/body/div[1]/div/h3",Target.XPATH);
	
	
	public static final Target newsletter_email = new Target("contact_us_newsletter_email","//*[@id='field0']",Target.XPATH);
	public static final Target newsletter_firstname = new Target("contact_us_newsletter_firstname","//*[@id=\"field1\"]",Target.XPATH);
	public static final Target newsletter_lastname = new Target("newsletter_lastname","//*[@id=\"field2\"]",Target.XPATH);
	public static final Target newsletter_address = new Target("newsletter_address","//*[@id=\"field3\"]",Target.XPATH);
	public static final Target newsletter_suite = new Target("newsletter_suite","//*[@id=\"field4\"]",Target.XPATH);
	public static final Target newsletter_city = new Target("newsletter_city","//*[@id=\"field5\"]",Target.XPATH);
	public static final Target newsletter_state = new Target("newsletter_state","//*[@id=\"field6\"]",Target.XPATH);
	public static final Target newsletter_state_select = new Target("newsletter_state_select","//*[@id=\"field6\"]/optgroup[1]/option[53]",Target.XPATH);
	public static final Target newsletter_zipcode = new Target("newsletter_zipcode","//*[@id=\"field7\"]",Target.XPATH);
	public static final Target newsletter_country = new Target("newsletter_country","//*[@id=\"field8\"]",Target.XPATH);
	public static final Target newsletter_country_select = new Target("newsletter_country_select","//*[@id=\"field8\"]/option[2]",Target.XPATH);
	public static final Target newsletter_occupation = new Target("newsletter_occupation","//*[@id=\"field9\"]",Target.XPATH);
	public static final Target newsletter_occupation_select = new Target("newsletter_occupation_select","//*[@id=\"field9\"]/option[33]",Target.XPATH);
	public static final Target newsletter_submit_btn = new Target("newsletter_submit_btn","//*[@id=\"form42\"]/div[12]/div/button",Target.XPATH);
	
	public static final Target global_toggle = new Target("global_toggle","//*[@id=\"gb-8a9t0w5t2-c--click-to-toggle\"]",Target.XPATH);
	public static final Target homepage_global_banner = new Target("homepage_global_banner","//*[@id=\"gb-8a9t0w5t2-c--global-banner\"]",Target.XPATH);
	public static final Target homepage_global_nav = new Target("homepage_global_nav","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]",Target.XPATH);
	public static final Target homepage_hero_carousel = new Target("homepage_hero_carousel","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div/div/div[1]/section",Target.XPATH);
	public static final Target homepage_promo = new Target("homepage_promo","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div/div/div[2]",Target.XPATH);
	public static final Target homepage_footer = new Target("homepage_footer","//*[@id=\"kp-page-outer\"]/div/footer/ul",Target.XPATH);
	
	public static final Target search_filter = new Target("search_filter","//*[@id=\"productResultsList\"]/div[1]/section/header",Target.XPATH);
	public static final Target search_sort = new Target("search_sort","//*[@id=\"productResultsSort\"]/div",Target.XPATH);
	public static final Target search_grid = new Target("search_results_grid","//*[@id=\"productResultsList\"]/div[2]",Target.XPATH);
	
	public static final Target pdp_breadcrumb = new Target("pdp_breadcrumb","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[1]/div[1]",Target.XPATH);
	public static final Target pdp_maindisplayimage = new Target("pdp_maindisplayimage","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[1]/div[1]/div[1]/div/div/div[1]/div/div",Target.XPATH);
	public static final Target pdp_thumbnails = new Target("pdp_thumbnails","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[1]/div[1]/div[2]",Target.XPATH);
	public static final Target pdp_productinfo = new Target("pdp_productinfo","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[1]/div[2]",Target.XPATH);
	public static final Target pdp_finddistributor = new Target("pdp_finddistributor","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[1]/div[2]/div/a",Target.XPATH);
	public static final Target pdp_requestquote = new Target("pdp_requestquote","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[1]/div[2]/div/ul/li[1]/a",Target.XPATH);
	public static final Target pdp_getaspec = new Target("pdp_getaspec","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[1]/div[2]/div/ul/li[2]/a",Target.XPATH);
	public static final Target pdp_tabs = new Target("pdp_tabs","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[2]/div[1]",Target.XPATH);
	public static final Target pdp_techdocuments = new Target("pdp_techdocuments","//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[2]/div[2]",Target.XPATH);
	
	public static final Target health_article1 = new Target("health_article1","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[2]",Target.XPATH);
	public static final Target health_article1_image = new Target("health_article1_image","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[2]/div/div/div[1]/div/picture/img",Target.XPATH);
	public static final Target health_article2 = new Target("health_article2","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[3]",Target.XPATH);
	public static final Target health_article2_image = new Target("health_article2_image","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[3]/div/div/div[1]/div/picture/img",Target.XPATH);
	public static final Target health_article3 = new Target("health_article3","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[4]",Target.XPATH);
	public static final Target health_article3_image = new Target("health_article3_image","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[4]/div/div/div[1]/div/picture/img",Target.XPATH);
	public static final Target home_industries = new Target("home_industries","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]/ul/li[3]/div[1]/a[1]/span",Target.XPATH);
	public static final Target home_industries_healthcare = new Target("home_industries_healthcare","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]/ul/li[3]/div[2]/ul/li[1]/a",Target.XPATH);
	
	public static final Target home_resources = new Target("home_resources","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]/ul/li[4]/div[1]/a[1]/span",Target.XPATH);
	//*[@id="kp-page-outer"]/div/header/nav/div[2]/div/div[2]/ul/li[4]/div[1]/a[1]/span
	
	public static final Target home_resources_literature = new Target("home_resources_literature","//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]/ul/li[4]/div[2]/ul/li[2]/a",Target.XPATH);
	public static final Target literature_results = new Target("literature_results","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div/div[2]/div[2]",Target.XPATH);
	public static final Target literature_filter = new Target("literature_filter","//*[@id=\"kp-page-outer\"]/div/section/div[2]/div/div[1]/div[1]/select",Target.XPATH);
	public static final Target literature_filter_select = new Target("literature_filter_select","//*[@id='caseStudy']",Target.XPATH);
	public static final Target KohlerIndustrialWorldWide = new Target("KohlerIndustrialWorldWide","//*[@id='gb-8a9t0w5t2-c--click-to-toggle']",Target.XPATH);
	
	IndustrialData industrialData = IndustrialData.fetch("IndustrialData");
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	String browserName = caps.getBrowserName();
	
	public Hippo_IndustrialPage atIndustrialPage()
	{
		try 
		{
			if(caps.getBrowserName().equals("MicrosoftEdge"))
			{
				getCommand().driver.navigate().to("javascript:document.getElementById('overridelink').click()");
			}
			//Retrieving page state
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			String test = js.executeScript("return document.readyState").toString();
			//Verifying page load
			if (test.equalsIgnoreCase("complete")) {
				Assert.assertEquals(getCommand().driver.getTitle(), industrialData.page_Title);
				log ("Industries Page loaded",LogType.STEP);
			}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Search page for search term resulting no results
	public Hippo_IndustrialPage verifyNoResults()
	{
		try
		{
			IndustrialData industrialData = IndustrialData.fetch("IndustrialData");
			log("Search input: "+industrialData.search_NoResult+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_industrial, industrialData.search_NoResult);
			getCommand().click(search_btn_industrial);
			
			if(caps.getBrowserName().equals("MicrosoftEdge") || caps.getBrowserName().equals("safari"))
			{
				Assert.assertEquals(industrialData.search_Header, getCommand().getText(search_header).toUpperCase(),"Search Results page did not open");
			}
			else
			{
				Assert.assertEquals(industrialData.search_Header, getCommand().getText(search_header),"Search Results page did not open");
			}
			log("Search Results page opened",LogType.STEP);
			Assert.assertEquals("0 results for \""+industrialData.search_NoResult+"\"", getCommand().getText(search_results_span),"Search results returned for no results search term");
			log("0 results for: "+industrialData.search_NoResult+" verified",LogType.STEP);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Search page for Industrial Product search
	public Hippo_IndustrialPage verifyProductResults()
	{
		try
		{
			IndustrialData industrialData = IndustrialData.fetch("IndustrialData");
			log("Search input: "+industrialData.search_Product+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_industrial, industrialData.search_Product);
			getCommand().click(search_btn_industrial);
			getCommand().waitFor(8);
			
			if(getCommand().driver.getCurrentUrl().trim().toLowerCase().contains("APM802".toLowerCase()))
				
			{
				
				log("Search Results page opened",LogType.STEP);

					boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/product/"+industrialData.search_Product.toLowerCase());
					Assert.assertEquals(status_PDP, true,"Product page opened for: "+ industrialData.search_Product);
					log("Product page opened for: "+ industrialData.search_Product,LogType.STEP);
					
					
			}
			else
			{
				boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/product/"+industrialData.search_Product.toLowerCase());
				Assert.assertEquals(status_PDP, true,"Product page not opened for: "+ industrialData.search_Product);
				log("Product page not opened for: "+ industrialData.search_Product,LogType.ERROR_MESSAGE);
			}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Distributor Locator on Industrial Page
	public Hippo_IndustrialPage verifyDistributorLocator()
	{
		try
		{
			getCommand().click(distributor_locator);
			getCommand().waitFor(5);
			
			Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
			String browserName = caps.getBrowserName();

			if(browserName.equals("chrome"))
			{
				if(isAlertPresent())
				{
					Alert alert = getCommand().driver.switchTo().alert();
					alert.dismiss();
				}
			}
			Assert.assertEquals(getCommand().getText(distributor_header).toUpperCase().trim(), industrialData.dist_Header,"Distributor Locator Page did not open");
			log("Distributor Locator Page opened",LogType.STEP);
			
			getCommand().clear(distributor_zipCode) .sendKeys(distributor_zipCode, industrialData.dist_ZipCode);
			getCommand().click(distributor_search_btn);
			log("Entered Zip Code and clicked on Search button",LogType.STEP);
			
			getCommand().waitFor(5);
			String search_results = getCommand().getText(distributor_search_results);
			log("Search Results: "+search_results,LogType.STEP);
			getCommand().clear(distributor_zipCode).sendKeys(distributor_zipCode, industrialData.dist_ZipCode1);
			getCommand().click(distributor_search_btn);
			log("Entered Another Zip Code and clicked on Search button",LogType.STEP);
			
			getCommand().waitFor(10);
			getCommand().waitForTargetPresent(distributor_search_results);
			log("Search Results: "+getCommand().getText(distributor_search_results),LogType.STEP);
			Assert.assertNotEquals(getCommand().getText(distributor_search_results), search_results,"Results & Map not updated");
			log("Results & Map updated and verified",LogType.STEP);
			
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Contact Us on Industrial page
	public Hippo_IndustrialPage verifyContactUs()
	{
		try
		{
			IndustrialData industrialData = IndustrialData.fetch("IndustrialData");
			getCommand().scrollTo(contact_us_industrial);
			log("Click on Send Us a Message under Contact Us",LogType.STEP);
			
			if(browserName.equals("safari"))
        	{
				getCommand().click(send_message_industrial);

        	}
        	else
        	{
    			getCommand().sendKeys(send_message_industrial, Keys.chord(Keys.CONTROL,Keys.RETURN));

        	}

			getCommand().waitFor(4);
			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			getCommand().waitFor(1);
			if(listofTabs.size()>1)
			{
    				getCommand().driver.switchTo().window(listofTabs.get(1));

			}
			pageLoad();
			Assert.assertTrue(getCommand().getPageTitle().contains(industrialData.contact_Page_Title),"Contact Us page not opened");
			log( getCommand().getPageTitle()+" page Opened",LogType.STEP);
			
			getCommand().waitFor(5);
			//Filling Send Us a Message Form
			log("Filling the Contact Us form",LogType.STEP);
			getCommand().sendKeys(contact_us_first_name, industrialData.contact_us_firstname);
			getCommand().sendKeys(contact_us_last_name, industrialData.contact_us_lastname);
			getCommand().sendKeys(contact_us_email, industrialData.contact_us_email);
			getCommand().sendKeys(contact_us_phone, industrialData.contact_us_phone);
			getCommand().sendKeys(contact_us_address, industrialData.contact_us_address);
			getCommand().sendKeys(contact_us_address2, industrialData.contact_us_address2);
			getCommand().sendKeys(contact_us_city, industrialData.contact_us_city);
			getCommand().click(contact_us_state);
			getCommand().click(contact_us_state_select);
			getCommand().sendKeys(contact_us_postalcode, industrialData.contact_us_postalcode);
			getCommand().sendKeys(contact_us_company, industrialData.contact_us_company);
			getCommand().click(contact_us_describe);
			getCommand().click(contact_us_describe_select);
			getCommand().sendKeys(contact_us_comments, industrialData.contact_us_comments);
			
			//Captcha implementation not done
			log("Closing Contact Us page and switching to Main page",LogType.STEP);
			
			
		          
           //E Newsletter
			
	            getCommand().click(contact_us_newsletter);
	            getCommand().waitFor(2);

			 log("E Newsletter Page opened",LogType.STEP);

            ArrayList<String> listofTabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());

            	getCommand().driver.switchTo().window(listofTabs2.get(0));


       //Filling up NewsLetter
       log("Filling the Newsletter form",LogType.STEP);

      
       getCommand().sendKeys(newsletter_firstname, industrialData.news_firstname);
       getCommand().sendKeys(newsletter_lastname, industrialData.news_lastname);
       getCommand().sendKeys(newsletter_address, industrialData.news_address);
       getCommand().sendKeys(newsletter_suite, industrialData.news_email);
       getCommand().sendKeys(newsletter_city, industrialData.news_city);
       getCommand().sendKeys(newsletter_zipcode, industrialData.news_zipcode);      
       getCommand().sendKeys(newsletter_email, industrialData.news_email);
       log("Submitting the Newsletter form",LogType.STEP);
       getCommand().click(newsletter_submit_btn);
       
       log("Closing E Newsletter page and switching to Main page",LogType.STEP);

		getCommand().driver.close();
			
          getCommand().driver.switchTo().window(listofTabs2.get(1));
		
           
    		//Find a Dealer
            log("Click on Find a Dealer under Contact Us",LogType.STEP);
        	if(browserName.equals("safari"))
			{
	            getCommand().click(contact_us_find_dealer);
			}
			else
			{
		           getCommand().sendKeys(contact_us_find_dealer, Keys.chord(Keys.CONTROL,Keys.RETURN));
			}
 
            getCommand().waitFor(5);
			ArrayList<String> listofTabs1 = new ArrayList<String> (getCommand().driver.getWindowHandles());
			getCommand().waitFor(1);
			if(listofTabs1.size()>1)
			{

    				getCommand().driver.switchTo().window(listofTabs1.get(1));


			}
			pageLoad();
			getCommand().waitForTargetVisible(distributor_header);
			Assert.assertEquals(getCommand().getText(distributor_header).toUpperCase().trim(), industrialData.dist_Header,"Dealer Locator Page did not open");
			log("Dealer Locator Page opened",LogType.STEP);
			log("Closing Find a Dealer page and switching to Main page",LogType.STEP);
			
			if(listofTabs1.size()>1)
			{
			getCommand().driver.close();
 
                getCommand().driver.switchTo().window(listofTabs1.get(0));

			}
			else
			{
				getCommand().driver.navigate().back();
			}
			
			
			
			
		}        
            
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Footer links on Industrial Page
	public Hippo_IndustrialPage VerifyFooter()
	{
		log("Verifying footer has 6 columns",LogType.STEP);
        // get the list of links available in footer
        WebElement footer_count = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/footer/ul"));
        List<WebElement> footer_headers = footer_count.findElements(By.className("menu-item__header"));
        WebElement footer_social = footer_count.findElement(By.className("menu-social__header"));
        footer_headers.add(footer_social);
        if(footer_headers.size() == 6)
        {
        	log("Footer has 6 columns",LogType.STEP);
        	for(WebElement ele:footer_headers) 
        	{
        		log(ele.getText(),LogType.STEP);
        	}
        }
        
        else
        {
        	log("Footer does not have 6 columns",LogType.ERROR_MESSAGE);
        	Assert.fail("Footer does not have 6 columns");
        }     
        try 
        {
        	WebElement footer_header_links = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/footer/ul"));
            List<WebElement> header_sub_menu_count = footer_header_links.findElements(By.className("menu-item__sub-menu"));
            WebElement header_sub_menu_social_count = footer_header_links.findElement(By.className("menu-social__sub-menu"));
            header_sub_menu_count.add(header_sub_menu_social_count);
            List<WebElement> footer_links_count = new ArrayList<WebElement>();
        	
            for(WebElement ele:header_sub_menu_count)
            {
            	List<WebElement> header_sub_menu_links_count = ele.findElements(By.tagName("li"));
            	for(WebElement elem:header_sub_menu_links_count)
            	{
            		footer_links_count.add(elem);
            	}
            }
            
            for(int count = 0; count < footer_links_count.size();count++)
            {
            	WebElement footer = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/footer/ul"));
            	List<WebElement> header_sub_menu = footer.findElements(By.className("menu-item__sub-menu"));
                WebElement header_sub_menu_social = footer.findElement(By.className("menu-social__sub-menu"));
                header_sub_menu.add(header_sub_menu_social);
                List<WebElement> footer_links = new ArrayList<WebElement>();
            	
                for(WebElement ele:header_sub_menu)
                {
                	List<WebElement> header_sub_menu_links = ele.findElements(By.tagName("li"));
                	for(WebElement elem:header_sub_menu_links)
                	{
                		footer_links.add(elem);
                	}
                }
            	String elemText = footer_links.get(count).getText();
            	if (!(elemText.equals(industrialData.notalink) || elemText.equals("Kohler Power Resource Center")))
    			{
    				WebElement menu_link = footer_links.get(count).findElement(By.tagName("a"));
        			getCommand().scrollTo(contact_us_industrial);
        			String pageTitle = getCommand().getPageTitle();
        			log("Clicking on the link: "+menu_link.getText(),LogType.STEP);
        			if(browserName.equals("safari"))
        			{
            			menu_link.sendKeys(Keys.chord(Keys.COMMAND,Keys.RETURN));

        			}
        			else
        			{
            			menu_link.sendKeys(Keys.chord(Keys.CONTROL,Keys.RETURN));

        			}
        			
        			getCommand().waitFor(4);
        			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
        			getCommand().waitFor(1);
        			log("Switching to "+elemText+" page",LogType.STEP);
        			if(listofTabs.size() > 1)
        			{
        				
        				if(browserName.equals("safari"))
            			{
                            getCommand().driver.switchTo().window(listofTabs.get(0));

            			}
            			else
            			{
                            getCommand().driver.switchTo().window(listofTabs.get(1));

            			}
        				
        				
                        pageLoad();
        			}
                    Assert.assertNotEquals(pageTitle, getCommand().getPageTitle(),"Footer link: "+elemText+" didnt open");
                    log("Switching back to main page",LogType.STEP);
                    if(listofTabs.size() > 1)
        			{
                        getCommand().driver.close();
                        
                        if(browserName.equals("safari"))
            			{
                            getCommand().driver.switchTo().window(listofTabs.get(1));

            			}
            			else
            			{
                            getCommand().driver.switchTo().window(listofTabs.get(0));

            			}
                        
                        
        			}
                    else
                    {
                    	getCommand().driver.navigate().back();
                    }

    			}
    			else
    			{
    				log(elemText+" is not a hyperlink",LogType.ERROR_MESSAGE);
    			}
            }
        }
        catch(Exception ex)
        {
			Assert.fail(ex.getMessage());
        }
		return this;
	}
	
	//Verify Industrial Home Page
	public Hippo_IndustrialPage VerifyHomePage()
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(getCommand().driver,10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"gb-8a9t0w5t2-c--click-to-toggle\"]")));

			//Global Banner
			getCommand().waitForTargetVisible(global_toggle);
			WebElement banner_text = getCommand().driver.findElement(By.xpath("//*[@id=\"gb-8a9t0w5t2-c--click-to-toggle\"]"));
			if (getCommand().isTargetVisible(homepage_global_banner) && banner_text.getText().trim().equals(industrialData.banner_text))
			{
				log("Global Banner "+banner_text.getText()+" is displayed",LogType.STEP);
			}
			else
			{
				log(industrialData.banner_text+" is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail(industrialData.banner_text+" is not displayed");
			}
			
			//Kohler Logo
			if (getCommand().isTargetVisible(homepage_global_banner) && banner_text.getText().trim().equals(industrialData.banner_text))
			{
				log("Kohler Logo is displayed",LogType.STEP);
			}
			else
			{
				log("Kohler Logo is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Kohler Logo is not displayed");
			}
			
			//Utility Navigation
			WebElement utility_nav = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]"));
			List<WebElement> utility_menu_links = utility_nav.findElements(By.tagName("li"));
			log("Utility Navigation bar consisting of below is displayed:",LogType.STEP);
			for (WebElement ele:utility_menu_links)
			{
				WebElement display_text = ele.findElement(By.tagName("a"));
				if (utility_nav.isDisplayed() && ele.isDisplayed())
				{
					log(display_text.getText(), LogType.SUBSTEP);
				}
			}
			
			//Search
			WebElement search_box = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[3]"));
			if (search_box.isDisplayed())
			{
				log("Search Box is displayed",LogType.STEP);
			}
			else
			{
				log("Search Box is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Box is not displayed");
			}
			
			//Carousel
			if (getCommand().isTargetVisible(homepage_hero_carousel))
			{
				log("Hero Carousel is displayed",LogType.STEP);
			}
			else
			{
				log("Hero Carousel is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Carousel is not displayed");
			}
			
			//Promo		
			if(getCommand().isTargetVisible(homepage_promo))
			{
				log("Promo modules are displayed",LogType.STEP);
			}
			else
			{
				log("Promo modules are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Promo modules are not displayed");
			}
			
			//Footer
			if(getCommand().isTargetVisible(homepage_footer))
			{
				log("Footer is displayed",LogType.STEP);
			}
			else
			{
				log("Footer is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Footer is not displayed");
			}
		}
		catch( Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Industrial Search Page
	public Hippo_IndustrialPage verifySearchPage()
	{
		try
		{
			IndustrialData industrialData = IndustrialData.fetch("IndustrialData");
			log("Search input: "+industrialData.search_Result+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_industrial, industrialData.search_Result);
			getCommand().click(search_btn_industrial);
			
			Assert.assertEquals(industrialData.search_Header.equalsIgnoreCase(getCommand().getText(search_header).trim()),true,"Search Results page did not open");
			log("Search Results page opened",LogType.STEP);
			
			getCommand().waitFor(5);
			
			WebElement tablist = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/section/div[2]/div[1]/ul"));
			
			//Search Tabs
			if (tablist.isDisplayed())
			{
				log("Search Results tab contains following tabs:",LogType.STEP);
				List<WebElement> tabs = tablist.findElements(By.tagName("li"));
				tabs.get(0).click();
				for(WebElement ele:tabs)
				{
					log(ele.getText(),LogType.SUBSTEP);
				}
			}
			else
			{
				log("Search Results tab not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Results tab not displayed");
			}
			
			//Search Filters & Categories
			if (getCommand().isTargetVisible(search_filter))
			{
				log("Search Filter is present and has following categories:",LogType.STEP);
				WebElement categories = getCommand().driver.findElement(By.xpath("//*[@id=\"productResultsList\"]/div[1]/section/div"));
				List<WebElement> categories_list = categories.findElements(By.className("facet-group-header"));
				
				for (WebElement ele:categories_list)
				{
					log(ele.getText(),LogType.SUBSTEP);
				}
			}
			else
			{
				log("Search Filters not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Filters not displayed");
			}
			
			//Sort by
			if (getCommand().isTargetVisible(search_sort))
			{
				log("Sort by dropdown is present:",LogType.STEP);
			}
			else
			{
				log("Sort by dropdown not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Sort by dropdown not displayed");
			}
			
			//Results Grid
			if(getCommand().isTargetVisible(search_grid))
			{
				log("Search Results grid is displayed:",LogType.STEP);
			}
			else
			{
				log("Search Results grid not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Search Results grid not displayed");
			}			
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Industrial PDP page
	public Hippo_IndustrialPage verifyPDP()
	{
		try
		{
			IndustrialData industrialData = IndustrialData.fetch("IndustrialData");
			log("Search input: "+industrialData.pdp_product+" entered and clicked on Search button",LogType.STEP);
			getCommand().sendKeys(search_input_industrial, industrialData.pdp_product);
			getCommand().click(search_btn_industrial);
			getCommand().waitFor(10);
			pageLoad();
			boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/product/"+industrialData.pdp_product.toLowerCase());
			Assert.assertEquals(status_PDP, true,"Product page for: "+ industrialData.pdp_product+" did not open");
			log("Product page opened for: "+ industrialData.pdp_product,LogType.STEP);
			
			//Breadcrumb
			if(getCommand().isTargetVisible(pdp_breadcrumb))
			{
				log("Breadcrumb is displayed",LogType.STEP);
			}
			else
			{
				log("Breadcrumb is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Breadcrumb is not displayed");
			}
			
			//Main display image 
			if(getCommand().isTargetVisible(pdp_maindisplayimage))
			{
				log("Main display image is displayed",LogType.STEP);
			}
			else
			{
				log("Main display image is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Main display image is not displayed");
			}
			
			//Thumbnails 
			if(getCommand().isTargetVisible(pdp_thumbnails))
			{
				log("Thumbnails are displayed",LogType.STEP);
			}
			else
			{
				log("Thumbnails are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Thumbnails are not displayed");
			}
			
			//Product Info 
			if(getCommand().isTargetVisible(pdp_productinfo) && getCommand().getText(pdp_productinfo).contains(industrialData.pdp_product))
			{
				log("Model# & Product Info is displayed:",LogType.STEP);
				log(getCommand().getText(pdp_productinfo),LogType.SUBSTEP);
			}
			else
			{
				log("Model# & Product Info is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Model# & Product Info is not displayed");
			}
			
			//Find a Distributor, Request a Quote & Get a Spec
			if(getCommand().isTargetVisible(pdp_finddistributor) && getCommand().isTargetVisible(pdp_requestquote) && getCommand().isTargetVisible(pdp_getaspec))
			{
				log("Find a Distributor, Request a Quote & Get a Spec are displayed",LogType.STEP);
			}
			else
			{
				log("Find a Distributor, Request a Quote & Get a Spec are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Find a Distributor, Request a Quote & Get a Spec are not displayed");
			}
			
			//Features, Controllers & Accessories
			List<String> productTabs = new ArrayList<String>();
			List<String> actualProductTabs = new ArrayList<String>();
			productTabs.add(industrialData.pdp_Tab1);
			productTabs.add(industrialData.pdp_Tab2);
			productTabs.add(industrialData.pdp_Tab3);
			
			if(getCommand().isTargetVisible(pdp_tabs))
			{
				WebElement pdpTabs = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/section/div[1]/div[3]/div[2]/div[1]/ul"));
				List<WebElement> tabs = pdpTabs.findElements(By.tagName("li"));
				log("Below tabs are displayed:",LogType.STEP);
				for (WebElement ele:tabs)
				{
					actualProductTabs.add(ele.getText().trim());
					log(ele.getText(),LogType.SUBSTEP);
				}
				CompareTabs (productTabs, actualProductTabs);
			}
			else
			{
				log("Features, Controllers & Accessories tabs are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Features, Controllers & Accessories tabs are not displayed");
			}
			
			//Technical Documents 
			if(getCommand().isTargetVisible(pdp_techdocuments))
			{
				log("Technical Documents are displayed",LogType.STEP);
			}
			else
			{
				log("Technical Documents are not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Technical Documents are not displayed");
			}
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Industrial Article Page
	public Hippo_IndustrialPage verifyArticle()
	{
		try
		{
			log("Navigating to Healthcare Page",LogType.STEP);
			
			if(browserName.equals("safari"))
			{
				WebElement Home_Industries=getCommand().driver.findElement(By.xpath("//*[@id='kp-page-outer']/div/header/nav/div[2]/div/div[2]/ul/li[3]/div[1]/a[1]/span"));
				Actions act=new Actions(getCommand().driver);
				act.moveToElement(Home_Industries).build().perform();
			}
			else
			{
				getCommand().mouseHover(home_industries);

			}
			
			getCommand().click(home_industries_healthcare);
			getCommand().waitForTargetVisible(health_article1);
			
			log("Below articles are displayed",LogType.STEP);
			pageLoad();
			//First Article
			if (getCommand().isTargetPresent(health_article1) && getCommand().isTargetPresent(health_article1_image))
			{
				WebElement article1_header = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/h1"));
				Assert.assertTrue(article1_header.getText().toUpperCase().trim().contains(industrialData.health_article1),"Article is not displayed");
				log("Article: "+article1_header.getText()+" is displayed",LogType.SUBSTEP);
				
			}
			else
			{
				log("Article is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Article is not displayed");
			}
			
			//Second Article
			if (getCommand().isTargetPresent(health_article2) && getCommand().isTargetPresent(health_article2_image))
			{
				WebElement article2_header = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[3]/div/div/div[2]/div/h1"));
				Assert.assertTrue(article2_header.getText().contains(industrialData.health_article2),"Article is not displayed");
				log("Article: "+article2_header.getText()+" is displayed",LogType.SUBSTEP);
				
			}
			else
			{
				log("Article is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Article is not displayed");
			}
			
			//Third Article
			if (getCommand().isTargetPresent(health_article3) && getCommand().isTargetPresent(health_article3_image))
			{
				WebElement article3_header = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/div[2]/div[1]/div/div/div[4]/div/div/div[2]/div/h1"));
				Assert.assertTrue(article3_header.getText().contains(industrialData.health_article3),"Article is not displayed");
				log("Article: "+article3_header.getText()+" is displayed",LogType.SUBSTEP);
				
			}
			else
			{
				log("Article is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Article is not displayed");
			}
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Industrial Literature page
	public Hippo_IndustrialPage verifyLiterature()
	{
		try
		{
			log("Navigating to Literature Page",LogType.STEP);
			Actions act=new Actions(getCommand().driver);
			WebElement Resources=getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/header/nav/div[2]/div/div[2]/ul/li[4]/div[1]/a[1]/span"));
			act.moveToElement(Resources).build().perform();
			//getCommand().mouseHover(home_resources);
			getCommand().click(home_resources_literature);	
			getCommand().waitFor(5);
			
			pageLoad();
			log("Filtering on the Literature page",LogType.STEP);
			
			WebElement LFilter=getCommand().driver.findElement(By.xpath("//*[@id='kp-page-outer']/div/section/div[2]/div/div[1]/div[1]/select"));
			act.moveToElement(LFilter).build().perform();
			//getCommand().mouseHover(literature_filter);
			getCommand().click(literature_filter);
             //getCommand().scrollTo(literature_filter_select);
			
			Select Filter = new Select(getCommand().driver.findElement(By.className("type")));
			Filter.selectByVisibleText("Case Study");
			//Filter.selectByIndex(1);

			
			
		//	getCommand().waitForTargetPresent(literature_filter_select);
			
			//getCommand().executeJavaScript("arguments[0].click();", literature_filter_select);
		//	getCommand().click(literature_filter_select);
			
			getCommand().waitFor(1);
			WebElement result_grid = getCommand().driver.findElement(By.xpath("//*[@id=\"kp-page-outer\"]/div/section/div[2]/div/div[2]/div[2]"));
			List<WebElement> results = result_grid.findElements(By.className("item"));
			List<String> width = new ArrayList<String>();
			
			log("Below Literature is displayed:",LogType.STEP);
			
		//	for (WebElement ele:results)
				for(int i=1;i<=results.size();i++)
			{
			//	if(!ele.getAttribute("style").contains(industrialData.ele_invisible))
					if(!results.get(i).getAttribute("style").contains(industrialData.ele_invisible))
				{
					WebElement pdf_link = results.get(i).findElement(By.tagName("svg"));
					if(pdf_link.isEnabled())
					{
						WebElement header = results.get(i).findElement(By.className("item__title"));
						width.add(results.get(i).getCssValue("width"));
						log(header.getText(),LogType.SUBSTEP);
						log("PDF link is present.Clicking on it and verifying it opens in a new tab.",LogType.INNER_SUBSTEP);
						results.get(i).click();
						
						getCommand().waitFor(5);
						System.out.println(getCommand().getPageUrl());

						ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
						getCommand().waitFor(1);
						
						if(browserName.equals("safari"))
						{
							if(i==1) {
								getCommand().driver.switchTo().window(listofTabs.get(0));
							}
							else
							{
							getCommand().driver.switchTo().window(listofTabs.get(1));
							}

						}
						else
						{
							getCommand().driver.switchTo().window(listofTabs.get(1));

						}
						getCommand().waitFor(5);
						Assert.assertTrue(getCommand().getPageUrl().contains(industrialData.file_type),"PDF did not open");
						getCommand().waitFor(2);
						getCommand().driver.close();
						if(browserName.equals("safari"))
						{
							if(i==1) {
								getCommand().driver.switchTo().window(listofTabs.get(1));
							}
							else
							{
							getCommand().driver.switchTo().window(listofTabs.get(0));
							}
								//getCommand().driver.switchTo().window(listofTabs.get(1));
							
						}
						else
						{
							getCommand().driver.switchTo().window(listofTabs.get(0));

						}
					}
					else
					{
						log("Literature is not present.",LogType.INNER_ERROR_MESSAGE);
						Assert.fail("Literature is not present");
					}
				}
			}
			CompareWidth(width);
			log("Sizes in Literature page are same",LogType.STEP);
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Hippo_IndustrialPage  VerifyHippoIndustrial_Comparefeature() throws InterruptedException
    {
		try
		{
			log("Navigating to Deisel generators page",LogType.STEP);
            Actions act=new Actions(getCommand().driver);
            WebElement productsHover = getCommand().driver.findElement(By.xpath("//span[contains(text(),'Products')]"));
            act.moveToElement(productsHover).build().perform();
            getCommand().waitFor(5);
                                    
            getCommand().driver.findElement(By.xpath("//a[contains(text(),'Diesel Generators')]")).click();
            getCommand().waitFor(5);
            JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
            if(caps.getBrowserName().equals("MicrosoftEdge"))
            {
            	js.executeScript("window.scrollBy(0,400)");
            }
            else
            {
            	js.executeScript("window.scrollBy(0,700)");
            }
            getCommand().waitFor(5);          
            String Parent=getCommand().driver.getWindowHandle();
            log("Listing all the products present in the page",LogType.STEP);
            List<WebElement> products=getCommand().driver.findElements(By.xpath("//section[contains(@class,'result-item')]"));
            for(int i=0;i<3;i++)
            {
            	getCommand().driver.switchTo().window(Parent);
            	WebElement element=products.get(i);
            	Actions sact=new Actions(getCommand().driver);
            	sact.moveToElement(element).build().perform();
            	getCommand().waitFor(5);
            	
            	log("Click on the compare link in the product",LogType.STEP);
            	List<WebElement> Compare=getCommand().driver.findElements(By.xpath("//label[@class='form-element__compare']"));
            	Compare.get(i).click();
            	Set<String> s1=getCommand().driver.getWindowHandles();
            	Iterator<String> it=s1.iterator();
            	while(it.hasNext())
            	{
            		String ChildWindow=it.next();
            		getCommand().driver.switchTo().window(ChildWindow);
            		log("switching to compare products window",LogType.STEP);
            	}
            	getCommand().waitFor(8);
            }
            if(products.size()==24)
            {
            	for(int l=1;l<=3;l++)
            	{
            		log("Verify compare panel shows three product modules (with thumbnail, product name, SKU# and price) each with a close icon",LogType.STEP);
            		getCommand().driver.findElement(By.xpath("(//div[@class='product-compare__image'])["+l+"]")).isDisplayed();
            		getCommand().driver.findElement(By.xpath("(//div[@class='product-compare__image'])["+l+"]/following-sibling::h3")).isDisplayed();
            		getCommand().driver.findElement(By.xpath("(//button[@class='closer'])["+l+"]")).isDisplayed();
            	}
            	log("Verify clear results link, compare CTA and expand arrow is displayed",LogType.STEP);
            	getCommand().driver.findElement(By.xpath("(//a[contains(text(),'Clear all')])[2]")).isDisplayed();
            	getCommand().driver.findElement(By.xpath("//a[contains(text(),'Compare')]")).isDisplayed();
            } 
            else
            {
            	log("Compare panel does not displays product modules",LogType.ERROR_MESSAGE);
            	Assert.fail("Compare panel does not displays product modules");
            }
            log("Click on Compare CTA link",LogType.STEP);
            getCommand().driver.findElement(By.xpath("//a[contains(text(),'Compare')]")).click();
            getCommand().waitFor(8);
            List<WebElement> productColumns =getCommand().driver.findElements(By.xpath("//div[@class='product-compare']/div"));
            Assert.assertEquals(4, productColumns.size(), "4 Columns are displayed in compare modal");
            
            for(int k=2;k<=4;k++)
            {
            	log("Verify Each product column has an image, product name, SKU# and price, and product features",LogType.STEP);
            	boolean images=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[2]/div["+k+"]")).isDisplayed();
            	boolean sku=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[1]/div["+k+"]")).isDisplayed();            	
            	boolean standbyRange=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[3]/div["+k+"]")).isDisplayed();            	
            	boolean primeRange=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[4]/div["+k+"]")).isDisplayed();            	
            	boolean continousRange=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[5]/div["+k+"]")).isDisplayed();            	
            	boolean fueltype=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[6]/div["+k+"]")).isDisplayed();           
            	boolean frequency=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[7]/div["+k+"]")).isDisplayed();            	
            	boolean speed=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[8]/div["+k+"]")).isDisplayed();            	
            	boolean alternateType=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[9]/div["+k+"]")).isDisplayed();            	
            	boolean engineManufacturer=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[10]/div["+k+"]")).isDisplayed();
            	boolean emissons=getCommand().driver.findElement(By.xpath("(//div[@class='cp-row'])[11]/div["+k+"]")).isDisplayed();
            	
            	if(images && sku && standbyRange  && primeRange && continousRange && fueltype && frequency && speed && alternateType && engineManufacturer && emissons ==true)
				{
					log("Each product column has an image, product name, SKU# and product features",LogType.STEP);
				}
				else
				{
					log("Missing elements from product features",LogType.ERROR_MESSAGE);
					Assert.fail("Missing elements from product features");
				}
            }                       	
		}
		catch(Exception e)
		{
			Assert.fail(e.getMessage());
		}
		return this;  
    }
	
	 public Hippo_IndustrialPage VerifyCategorypageforIndustrial()
	 {
		 try
		 {
			 log("Navigating to Deisel generators page",LogType.STEP);
			 Actions act=new Actions(getCommand().driver);
			 WebElement productsHover = getCommand().driver.findElement(By.xpath("//span[contains(text(),'Products')]"));
			 act.moveToElement(productsHover).build().perform();
			 getCommand().waitFor(5);
			 
			 getCommand().driver.findElement(By.xpath("//a[contains(text(),'Diesel Generators')]")).click();
			 getCommand().waitFor(8);
                         
			 WebElement cateogoryImage=getCommand().driver.findElement(By.xpath("//div[@class='kp-image']"));
                           
			 if(cateogoryImage.isDisplayed())
			 {
				 log("cateogoryImage is displayed", LogType.STEP);
			 }
			 else
			 {
				 log("cateogoryImage is not displayed", LogType.ERROR_MESSAGE);
				 Assert.fail("cateogoryImage is not displayed");
			 }

			 JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			 js.executeScript("window.scrollBy(0,400)");
			 getCommand().waitFor(5); 

			 WebElement categorypage=getCommand().driver.findElement(By.xpath("//div[@ng-app='kpProductSearch']"));

			 if(categorypage.isDisplayed())
			 {
				 log("category page is displayed", LogType.STEP);
			 }
			 else
			 {
				 log("category page is not displayed", LogType.ERROR_MESSAGE);
				 Assert.fail("category page is not displayed");
			 }

			 int X=getCommand().driver.manage().window().getSize().getWidth();

			 WebElement Filters=getCommand().driver.findElement(By.xpath("//header[@ng-model='facetsActive']/following-sibling::div"));

			 int x=Filters.getLocation().getX();

			 if(Filters.isDisplayed() && x<(X/2))
			 {
				 log("ThumbNails are displayed to the left of main image",LogType.STEP);
			 }
			 else
			 {
				 log("ThumbNails are not displayed to the left of main image",LogType.ERROR_MESSAGE);
				 Assert.fail("ThumbNails are not displayed to the left of main image");
			 }

			 List<WebElement> SortBy=getCommand().driver.findElements(By.xpath("//div[@ng-if='hasresults']/div[2]/ul/li"));

			 if(SortBy.size()!=0)
			 {
				 log("Sort by drop-down is present", LogType.STEP);
			 }
			 else
			 {
				 log("Sort by drop-down is not present", LogType.ERROR_MESSAGE);
				 Assert.fail("Sort by drop-down is not present");
			 }

			 List<WebElement> ProductGrid=getCommand().driver.findElements(By.xpath("//section[contains(@class,'result-item ')]"));

			 if(ProductGrid.size()!=0)
			 {
				 log("ProductGrid is present", LogType.STEP);
			 }
			 else
			 {
				 log("ProductGrid is not present", LogType.ERROR_MESSAGE);
				 Assert.fail("ProductGrid is not present");
			 }
		 }

		 catch(Exception e)
		 {
			 Assert.fail(e.getMessage());
		 }
         return this;
	 }
	 
	 public Hippo_IndustrialPage VerifyProductfilters()
     {   
		 try
		 {			 
			 log("Navigating to Diesel generators page",LogType.STEP);
			 Actions act=new Actions(getCommand().driver);
			 WebElement productsHover = getCommand().driver.findElement(By.xpath("//span[contains(text(),'Products')]"));
			 act.moveToElement(productsHover).build().perform();
			 getCommand().waitFor(5);
                                             
			 getCommand().driver.findElement(By.xpath("//a[contains(text(),'Diesel Generators')]")).click();
			 getCommand().waitFor(10);
			 String ProductResults = getCommand().driver.findElement(By.xpath("//*[@class=\"product-search__results__sort__count ng-binding\"]")).getText();
			 JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			 js.executeScript("window.scrollBy(0,700)");
			 getCommand().waitFor(5); 
			 
			 if(getCommand().getPageTitle().contains("Generators"))
				 Assert.assertTrue(true, "Successfully in Diesel Industrial Generators Page");
               	
                         
			 log("Verifying that filters for KW range",LogType.STEP);
			 getCommand().driver.findElement(By.xpath("(//span[contains(text(),'0-100')])[1]//preceding-sibling::input")).click();
			 getCommand().waitFor(5);
			 String KWRangeResults = getCommand().driver.findElement(By.xpath("//*[@class=\"product-search__results__sort__count ng-binding\"]")).getText();
			 
			 if((KWRangeResults)!=(ProductResults))
			 {
				 log("product grid updated accordingly using fuel type filters",LogType.STEP);
			 }
			 else
			 {
				 log("product grid is not updated using fuel type filters",LogType.ERROR_MESSAGE);
				 Assert.fail("product grid is not updated fuel type filters");
			 }
                         
		 }
		 catch(Exception ex)
		 {
			 Assert.fail(ex.getMessage());
			 ex.printStackTrace();
		 }
		 return this;
     }
     
	 public Hippo_IndustrialPage  VerifyHippoIndustrial_HeroImage() throws InterruptedException
     {
		 try
		 {
			 String HeroImage1 = "//div[@id='slick-slide00']/div/div/div[1]";
			 String HeroImage2 = "//div[@id='slick-slide01']/div/div/div[1]";
			 String HeroImage3 = "//div[@id='slick-slide02']/div/div/div[1]";
			 String HeroImage4 = "//div[@id='slick-slide03']/div/div/div[1]";
			 
			 getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			 log("Clicking on Navigation dot2 to get second hero image",LogType.STEP);
			 getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control01']")).click();
			 getCommand().waitFor(10);
			 getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			 if(!(HeroImage2==HeroImage1))
			 {
				 log("Hero Image changed After click on Navigation dots",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			 }
			 
			 log("Clicking on Navigation dot3 to get third hero image",LogType.STEP);
			 getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control02']")).click();
			 getCommand().waitFor(10);
			 getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			 if(!(HeroImage3==HeroImage2))
			 {
				 log("Hero Image changed After click on Navigation dots",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			 }
			 
			 log("Clicking on Navigation dot4 to get fourth hero image",LogType.STEP);
			 getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control03']")).click();
			 getCommand().waitFor(10);
			 getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			 if(!(HeroImage4==HeroImage3))
			 {
				 log("Hero Image changed After click on Navigation dots",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			 }
			 
			 log("Clicking on Navigation dot1 to get first hero image",LogType.STEP);
			 getCommand().driver.findElement(By.xpath("//button[@id='slick-slide-control00']")).click();
			 getCommand().waitFor(10);
			 getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			 if(!(HeroImage1==HeroImage4))
			 {
				 log("Hero Image changed After click on Navigation dots",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			 }
			 
			 getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			 if(!(HeroImage1==HeroImage4))
			 {
				 log("Hero Image changed After clicking on previous icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			 }              
			 getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			 if(!(HeroImage1==HeroImage2))
			 {
				 log("Hero Image changed After clicking on Next icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			 }
			 
			 getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			 if(!(HeroImage1==HeroImage2))
			 {
				 log("Hero Image changes After clicking on previous icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			 }              
			 getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			 if(!(HeroImage2==HeroImage3))
			 {
				 log("Hero Image changed After clicking on Next icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			 }
			 
			 getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			 if(!(HeroImage2==HeroImage3))
			 {
				 log("Hero Image changes After clicking on previous icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			 }              
			 getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			 if(!(HeroImage3==HeroImage4))
			 {
				 log("Hero Image changed After clicking on Next icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			 }
			 
			 getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_left')]")).click();
			 if(!(HeroImage3==HeroImage4))
			 {
				 log("Hero Image changed After clicking on previous icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on previous icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			 }
			 
			 getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			 getCommand().driver.findElement(By.xpath("//div[contains(@class,'kp-svg-slider_arrow_right')]")).click();
			 getCommand().waitFor(5);
			 getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			 if(!(HeroImage4==HeroImage1))
			 {
				 log("Hero Image changed After clicking on Next icon",LogType.STEP);
			 }
			 else
			 {
				 log("Hero Image remains unchanged After clicking on Next icon",LogType.ERROR_MESSAGE);
				 Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			 }
			 
		 }
		 catch(Exception e)
		 {
			 Assert.fail(e.getMessage());
		 }
		 
		 return this;
     }
	 
	 public Hippo_IndustrialPage Industrial_WorldWide() throws InterruptedException
	 {
		 try
		 {
			 List<String> WorldWideBannerRegionsList = new ArrayList<String>();                  
			 int  WorldWideBanner_Columnscount = 0;
			 
			 log("Checking worldwide banner for Industrial Site",LogType.STEP);
			 log("Clicking on KOHLER Industrial Banner",LogType.STEP);
	                  
			 getCommand().isTargetPresent(KohlerIndustrialWorldWide);
			 getCommand().click(KohlerIndustrialWorldWide);
			 
			 log("Verifying WorldWide Banner is composed of 7 columns",LogType.STEP);

			 List<WebElement> WorldWideBanner = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/ul"));
			 
			 for(WebElement WorldWidebanner : WorldWideBanner)
			 {
				 if(WorldWidebanner.isDisplayed())
				 {
					 WorldWideBanner_Columnscount++;
				 }
			 }

			 if(WorldWideBanner_Columnscount == 7)
			 {
				 log("WorldWide Banner is displayed with 7 columns",LogType.STEP);
				 log("Checking WorldWide Banner is displayed with 7 different regions",LogType.STEP);
				 
				 List<WebElement> WorldWideBannerRegions = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/h3"));
				 log("Getting regions text and verifying all are of different regions",LogType.STEP);
				 
				 for (WebElement WorldWideBannerRegion : WorldWideBannerRegions)
				 {
					 WorldWideBannerRegionsList.add(WorldWideBannerRegion.getText());
				 }
	                 
				 Assert.assertTrue(CompareDataFromSameList(WorldWideBannerRegionsList),"WorldWide Banner is not displayed with 7 different regions");
				 log("WorldWide Banner is displayed with 7 different regions",LogType.STEP);                 
	                                 
				 List<WebElement> AllRegionLinksCount = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/ul/li/a"));				 
				 String Pageurl = getCommand().getPageUrl();			
				 log("Clicking on various links and verifying they are Navigated as expected",LogType.STEP);
				 
				 for(int count = 0; count < AllRegionLinksCount.size(); count++)
				 {	
					 List<WebElement> AllRegionLinks = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--desktop']/div/ul/li/a"));
					 String Linktext = AllRegionLinks.get(count).getText();
					 log("Clicking and opening the link "+Linktext+ " in new tab",LogType.STEP); 
					 
					 if(browserName.equals("safari"))
					 {
						 String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN);
						 AllRegionLinks.get(count).sendKeys(selectLinkOpeninNewTab);


					 }
					 else
					 {
						 String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
						 AllRegionLinks.get(count).sendKeys(selectLinkOpeninNewTab);


					 }
					

					 getCommand().waitFor(5);
					 ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
					 log("Switching to new tab",LogType.STEP);
					 
					 if(listofTabs.size() > 1)
					 {
						 if(browserName.equals("safari"))
						 {
							 getCommand().driver.switchTo().window(listofTabs.get(0));

						 }
						 else
						 {
							 getCommand().driver.switchTo().window(listofTabs.get(1));

						 }
						 
						 
						 
					 }
					 pageLoad();
					 log("Getting new tab page title",LogType.STEP);
					 String CurrentpageUrl = getCommand().getPageUrl();
					 
					 if(Pageurl.equals(CurrentpageUrl))
					 {	
						 log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
						 Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
					 }
					 else
					 {                                             
						 log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
					 }
					 if(listofTabs.size() > 1)
					 {
						 getCommand().driver.close();
						 
						 if(browserName.equals("safari"))
						 {
							 getCommand().driver.switchTo().window(listofTabs.get(1));

						 }
						 else
						 {
							 getCommand().driver.switchTo().window(listofTabs.get(0));

						 }
						 
					 }
					 else
					 {
						 getCommand().driver.navigate().back();
					 }
				 }      
			 }
		 }
		 catch(Exception ex)
		 {
			 Assert.fail(ex.getMessage());
		 }

		 return this;
	 }
	 
	//Helpers
	 //Compare Data from Lists
	 public boolean CompareDataFromSameList(List<String> list)
	 {
		 for (int i = 0; i < list.size()-1; i++) 
		 {
			 for (int k = i+1; k < list.size(); k++) 
			 {			
				 if(list.get(i).equals(list.get(k)))
				 {
					 Assert.fail("Mismatch in data present in the list");
				 }				      
			 }	      
		 }		
		 return this != null;
	 }
	 
	//Compare tabs
	public Hippo_IndustrialPage CompareTabs(List<String> expectedList, List<String> actualList)
	{
		if (expectedList.size() == actualList.size())
		{
			for (int i=0; i < actualList.size(); i++)
			{
				if (!expectedList.get(i).equalsIgnoreCase(actualList.get(i)))
				{
					Assert.fail("Mismatch in product Tabs");
				}
			}
		}	
		return this;
	}
	
	//Compare Width
	public Hippo_IndustrialPage CompareWidth(List<String> list)
	{
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(!list.get(i).equals(list.get(k)))
				{
					Assert.fail("Mismatch in sizes on Literature Page");
				}				      
			}	      
		}		
		return this;
	}
	
	//PageLoad
	public Hippo_IndustrialPage pageLoad()
	{
		try
		{
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			String test = js.executeScript("return document.readyState").toString();
			
			while (!test.equalsIgnoreCase("complete"))
			{
				getCommand().waitFor(1);
				test = js.executeScript("return document.readyState").toString();
			}
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Alert Handling
	public boolean isAlertPresent()
	{
		try
		{
			getCommand().driver.switchTo().alert();
			return true;
		}
		catch(NoAlertPresentException ex)
		{
			return false;
		}
	}
}
