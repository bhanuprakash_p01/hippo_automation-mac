package com.components.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import com.components.repository.SiteRepository;
import com.components.yaml.LatamxSearchData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;



public class Hippo_LatinAmerica extends SitePage
{

	/* Defining the locators on the Page */ 
	

	public static final Target Link_CADsymbols= new Target("link_CADsymbols","//*[@id='koh-page-outer']/div/footer/div[1]/div/div[2]/ul/li[4]/ul/li[1]/a",Target.XPATH);
    public static final Target Link_BidetsDWG= new Target("link_BidetsDWG","//*[@id=\"koh-page-outer\"]/div/div/div/div/div/div[2]/section/div/div/div[5]/div/div[2]/a[1]",Target.XPATH);
    public static final Target Link_BidetsDXF= new Target("link_BidetsDWG","//*[@id=\"koh-page-outer\"]/div/div/div/div/div/div[2]/section/div/div/div[5]/div/div[2]/a[2]",Target.XPATH);
    public static final Target Link_contactUs= new Target("link_Contact us","//*[@id='koh-page-outer']/div/footer/div[1]/div/div[2]/ul/li[3]/ul/li[3]/a",Target.XPATH);
    public static final Target Search = new Target("Search","(//input[@id='koh-nav-searchbox'])[2]",Target.XPATH);
    public static final Target SearchButton = new Target("Search Button","(//*[@id='koh-nav-searchbutton'])[2]",Target.XPATH);
    public static final Target DiscontinuedLatmax = new Target("DiscontinuedLatmax","//*[@id=\"koh-page-outer\"]/div/div/section/div[1]/div[3]/span[1]",Target.XPATH);
    public static final Target KohlerWorldwide = new Target("DiscontinuedLatmax","//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[1]/ul[2]/li[1]/a",Target.XPATH);
    public static final Target Mexico = new Target("Mexico","//*[@id=\"koh-page-outer\"]/div/header/div/div[1]/div[1]/div[2]/div[1]/ul/li[1]/ul/li[10]/a",Target.XPATH);
    public static final Target Bathroom =new Target("Bathroom","//*[@id='koh-primary-nav-menu']/ul/li[1]",Target.XPATH);
    public static final Target Literature_Footer=new Target("Literature From Footer","//*[@id='koh-page-outer']/div/footer/div[1]/div/div[2]/ul/li[3]/ul/li[2]/a",Target.XPATH);
    public static final Target Literature_Bathroom=new Target("Literature_Bathroom","//*[@id='koh-primary-nav-menu']/ul/li[1]/div/div/ul[4]/li[4]",Target.XPATH);
    public static final Target Numbers=new Target("Numbers and Emails","//*[@id='koh-page-outer']/div/div/div/div/div/div/div/section/div/div/ul/li[1]/ul/li[3]",Target.XPATH);
    public static final Target DiscontinuedMexico=new Target("Discontinued text for Mexico","//*[@id=\"koh-page-outer\"]/div/div/section/div[1]/div[3]/span[1]",Target.XPATH);
    public static final Target Module_Literature=new Target("Literature page modules","//*[@id=\"koh-page-outer\"]/div/div/section/div[1]/div[3]/span[1]",Target.XPATH);
    public static final Target WhereToBuyLink=new Target("Where To buy link","/html/body/div/div/header/div/div[1]/div[1]/div[1]/ul[1]/li[1]/a",Target.XPATH);
    public static final Target Urinales_module=new Target("Module","//*[@id='koh-page-outer']/div/div/div/div/div/div/section/div/div/div[1]/a",Target.XPATH);
    public static final Target WhereToBuyText=new Target("Where to buy text","//*[@id='wrap']/div[1]/div/h2",Target.XPATH);
    public static final Target SuggestionsContainer=new Target("SuggestionsContainer","//*[@id='search-suggestions-container']",Target.XPATH);
    public static final Target English=new Target("EN","//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[1]/ul[2]/li[2]/a[1]",Target.XPATH);
    public static final Target ProductSKU=new Target("sku","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[2]/span",Target.XPATH);
    public static final Target Kitchen=new Target("Kitchen","//*[@id='koh-primary-nav-menu']/ul/li[2]/a",Target.XPATH);
    public static final Target KitchenUberCategory=new Target("Kitchen"," //*[@id='koh-primary-nav-menu']/ul/li[2]/div/div/div/a",Target.XPATH);
    public static final Target HeroNextArrow=new Target("Hero Next Arrow","//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/button[2]",Target.XPATH);
    public static final Target HeroPrevArrow=new Target("Hero Prev Arrow","//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/button[1]",Target.XPATH);
    public static final Target QuickView=new Target("Quick View","//*[@id='koh-page-outer']/div/div/section/div[2]/div[2]/div/div[2]/div/button/span",Target.XPATH);
    public static final Target Mousehover=new Target("Quick View","//*[@id='koh-page-outer']/div/div/section/div[2]/div[2]/div/div[2]/div",Target.XPATH);
    public static final Target Literature_Module=new Target("Quick View","/html/body/div/div/div/div/div/div/div/section/div/div/div[5]/a",Target.XPATH);
    public static final Target CompareOverlay=new Target("Compare","//*[@id='comparePanel']/div/div[1]/a",Target.XPATH);
    public static final Target CompareOverlayArrow=new Target("Compare","//*[@id='comparePanel']/div/div[1]/button[1]",Target.XPATH);
    public static final Target ProductName=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[1]/div[1]",Target.XPATH);
    public static final Target PDPProductSKU=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[2]/span",Target.XPATH);
    public static final Target StoreLocator=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[4]/a",Target.XPATH);
    public static final Target Compare=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[5]/a[1]",Target.XPATH);
    public static final Target ShareButton=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[5]/div[1]/button",Target.XPATH);
    public static final Target Print=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[5]/div[2]/button",Target.XPATH);
    public static final Target PDPImage=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[1]/div[1]/div/div/div[1]/div[1]/div/img",Target.XPATH);
    public static final Target Thumbnails=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[1]/div[1]/div[2]/div",Target.XPATH);
    public static final Target Features=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[3]/div[1]/div[2]",Target.XPATH);
    public static final Target ServiceSupport=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[3]/div[2]/div[1]/span",Target.XPATH);
    public static final Target TechnicalInformation=new Target("ProductName","//*[@id='koh-page-outer']/div/div/section/div[3]/div[2]/div[2]/span",Target.XPATH);
    public static final Target close=new Target("ProductName","(//button[@class='remodal-close'])[142]",Target.XPATH);
    public static final Target KohlerWorldWide=new Target("KohlerWorldWide","//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[1]/ul[2]/li[1]/a",Target.XPATH);
    public static final Target button0 = new Target("carousel1","//*[@id='slick-slide00']/button",Target.XPATH);
	public static final Target button1 = new Target("carousel1","//*[@id='slick-slide01']/button",Target.XPATH);
	public static final Target button2 = new Target("carousel1","//*[@id='slick-slide02']/button",Target.XPATH);
	public static final Target button3 = new Target("carousel1","//*[@id='slick-slide03']/button",Target.XPATH);
	public static final Target button4 = new Target("carousel1","//*[@id='slick-slide04']/button",Target.XPATH);
	public static final Target IMG_SELECT = new Target("Image_select","//img[@alt='Image result for testing for images']",Target.XPATH);
	public static final Target SIGNOUT_ARROW=new Target("signout-arrow","//div[@id='gbw']/div/div/div[2]/div[4]/div/a/span",Target.XPATH);
	public static final Target LNK_SIGNOUT = new Target("lnk_signout","//*[@id='gb_71']",Target.XPATH);
	public static final Target MSG_NOTAVAILABLE = new Target("msg_notavailable","//*[@id='search_results_msg']",Target.XPATH);	
	public static final Target HEADER_SEARCH_INPUT = new Target("header-search-input","(//input[@id='koh-nav-searchbox'])[2]",Target.XPATH);
	public static final Target HEADER_SEARCH_BUTTON = new Target("header-search-button","(//button[@id='koh-nav-searchbutton'])[2]",Target.XPATH);
	public static final Target ALTERNATIVE_SEARCH_OPTION = new Target("alternative serach option","//div[@class='koh-hero-container']/h1[2]/a",Target.XPATH);
	public static final Target EN_LINK = new Target("EN link","(//a[@class='koh-link-language-left '])[1]",Target.XPATH);
	public static final Target ES_LINK = new Target("ES link","(//a[@class='koh-link-language-right koh-link-language-active'])[1]",Target.XPATH);
	public static final Target BATHROOM = new Target("Bathroom link","//ul[@class='koh-nav-parents']/li[1]/span",Target.XPATH);
	public static final Target KITCHEN = new Target("kitchen link","//ul[@class='koh-nav-parents']/li[2]/a",Target.XPATH);
	public static final Target IDEAS = new Target("Ideas link","//ul[@class='koh-nav-parents']/li[3]/a",Target.XPATH);
	public static final Target KITCHENITEM = new Target("kitchen item","(//ul[@class='koh-nav-section '])[5]/li[1]/a",Target.XPATH);
	public static final Target QUICKVIEWDISPLAY = new Target("Display Quickview","//div[@class='remodal-wrapper remodal-is-opened']/div",Target.XPATH);
	public static final Target PRODUCTIMAGE = new Target("Image of a product","(//div[@class='remodal-wrapper remodal-is-opened']/div)/div[1]",Target.XPATH);
	public static final Target QUICKVIEW = new Target("QuickView of a product","(//div[@class='koh-product-quick-view'])[2]/button",Target.XPATH);
	public static final Target PRODUCTCONTENT = new Target("Product content","(//div[@class='koh-product-content'])[2]",Target.XPATH);
	public static final Target COMPARECTA = new Target("Compare CTA","(//div[@class='koh-product-tools'])[2]",Target.XPATH);
	public static final Target SORTBY = new Target("sortBy","//button[@class='koh-sort-by']",Target.XPATH);
	public static final Target ProductId_PDP = new Target("Product Id","(//span[@class='koh-product-sku'])[1]",Target.XPATH);
	public static final Target SearchBox = new Target("Product Id","//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[2]/form",Target.XPATH);
	public static final Target noSearchResults = new Target("Product Id","//div[@class='koh-hero-container']/h1[1]",Target.XPATH);
	public static final Target didYouMeanFeature = new Target("Product Id","//div[@class='koh-hero-container']/h1[2]",Target.XPATH);
	public static final Target alternativeSearchOption = new Target("Product Id","//div[@class='koh-hero-container']/h1[2]/a",Target.XPATH);
	public static final Target productNumber = new Target("Product Id","//span[@class='koh-product-sku']",Target.XPATH);
	public static final Target content = new Target("Product Id","(//ul[@class='koh-nav-parents'])/li[1]",Target.XPATH);
	public static final Target contentEN = new Target("Product Id","(//ul[@class='koh-nav-parents'])/li[1]",Target.XPATH);
	public static final Target productDescription = new Target("Product Id","(//div[@class='koh-product-content'])[2]/span/a",Target.XPATH);
	public static final Target defaultCategory = new Target("Product Id","//button[@class='koh-selected-filter']/span[2]",Target.XPATH);

	
    JavascriptExecutor JS = (JavascriptExecutor)getCommand().driver;
    Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	 String browserName = caps.getBrowserName();


	public Hippo_LatinAmerica(SiteRepository repository)
    {
           super(repository);
    }
 
     //Launching the URL
	public Hippo_LatinAmerica atHippo_LatinAmerica()
    {
           log("Launched Kohler Site",LogType.STEP);
		return this;
    }
	
	//Verify CAD Symbols link functionality
	public Hippo_LatinAmerica verifyCADSymbolsPage()
	{
	 try
	  {
		getCommand().click(English);
		getCommand().waitFor(5);
		getCommand().click(Link_CADsymbols);
		getCommand().waitFor(3);
		String Linktext=getCommand().getText(Link_CADsymbols);
		
		String Url=getCommand().driver.getCurrentUrl();
		
		if(Url.contains("cad-symbols"))
		{
			log("clicking on the link"+Linktext+"is redirecting to CAD symbols page",LogType.STEP);
		}
		else
		{
			log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
			Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
		}
		
				
	 List<WebElement> tilesHeaders=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div"));
	 
	
	 for(WebElement tilesHeader:tilesHeaders)
	 {
		 
		String TilesText=tilesHeader.getText();
		log("Checking modules in CAD symbols page",LogType.STEP);
		
		if(TilesText.contains("All Symbols") || TilesText.contains("Bathtubs") || TilesText.contains("Shower and Bathtub Module")) 
		{
        log("Following modules '"+TilesText+"' are displayed in CAD symbols page",LogType.STEP);
        }
	    else
		{
            log("Expected Column text is not displayed for column " +TilesText+", in CAD symbols page",LogType.ERROR_MESSAGE);
            Assert.fail("Expected Column text is not displayed for column " +TilesText+", in CAD symbols page");
        }
	  }
	}catch(Exception ex)
	 {
		Assert.fail(ex.getMessage());
	 }
	
	 return this;
	}
	
	//Verify Contact Us link functionality
	public Hippo_LatinAmerica verifyContactUsPage()
	 {
		try
		{
			getCommand().click(English);
			getCommand().waitFor(5);
			getCommand().click(Link_contactUs);
			getCommand().waitFor(3);
			String Linktext=getCommand().getText(Link_contactUs);
			log("clicking on the link" +Linktext,LogType.STEP);
			String currentPageTitle= getCommand().driver.getTitle();
			System.out.println(currentPageTitle);
			String currentPageUrl=getCommand().driver.getCurrentUrl();
			System.out.println(currentPageUrl);
			
			if(currentPageTitle.contains("Contact Us") && currentPageUrl.contains("contact-us-page"))
			{
				log("clicking on the link" +Linktext+" is redirecting to Contact us page",LogType.STEP);
			}
			else
			{
				log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
				Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
			}

			List<WebElement> ContactUsPage = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div/div/section"));
			for(WebElement ArticlePage:ContactUsPage)
			 {
				 
				String ArticlePageText=ArticlePage.getText();
				log("Checking for Email and Phone numbers in Contact Us page",LogType.STEP);
				
            if(ArticlePageText.contains("Phone") && ArticlePageText.contains("E-mail") )
            {
                   log("Text with Phone numbers and email address are present in Contact us page",LogType.STEP);
            }
            
            else
            {
                   log("No information present in Contact Us page",LogType.ERROR_MESSAGE);
                   Assert.fail("No information present in Contact Us page");
            }
	      }
		}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
          
               return this;
	}
	
	//verify Discontinued SKUs
	public Hippo_LatinAmerica verifyDiscontinuedSKUInLA(String keyword)
	 {
	    try {
	    	
	    	LatamxSearchData search = LatamxSearchData.fetch(keyword);
			String Text = search.keyword;
			log("Clicking on search",LogType.STEP);		
			getCommand().isTargetVisible(Search);
	    	getCommand().click(Search);
	    	log("Passing sku T10274-4-CP to the search box",LogType.STEP);
	    	getCommand().sendKeys(Search, Text);
        			
            getCommand().waitForTargetPresent(SearchButton);
	         	
	        	 log("Clicking on Search button",LogType.STEP);
	             getCommand().click(SearchButton);       
		         getCommand().waitFor(8);
	           
	            String currentPageUrl=getCommand().driver.getCurrentUrl();
	            		
	          if(currentPageUrl.contains("skuid=K-T10274-4-CP"))
	            {
	            	log("Product 'T10274-4-CP' is displayed",LogType.STEP);
	            }
	          else
	            {
	             	log("Product T10274-4-CP is not displayed",LogType.ERROR_MESSAGE);
	             	Assert.fail("Product T10274-4-CP is not displayed");
	            }
	          
	          String DiscontinuedText=getCommand().getText(DiscontinuedLatmax);
	    	  if(DiscontinuedText.contains("Este producto ha sido descontinuado."))
	    	  	 {
	            	log("Discontinued template displays",LogType.STEP);
	             }
	           else
	             {
	            	log("Discontinued template is not displayed",LogType.ERROR_MESSAGE);
	            	Assert.fail("Discontinued template is not displayed");
	             }
	            	    		
	         }

	  catch(Exception ex)
      {
	         Assert.fail(ex.getMessage());
      }
	  return this;		
	 }
   	 
	//verify Discontinued SKU In Mexico
	public Hippo_LatinAmerica verifyDiscontinuedSKUInMexico(String keyword)
	 {
	    try {
	    	
	    	LatamxSearchData search = LatamxSearchData.fetch(keyword);
			String Text = search.keyword;
	        log("Clicking on Mexico from KohlerWorldwide ",LogType.STEP);
	        getCommand().click(KohlerWorldwide);
	        getCommand().waitFor(5);
	        getCommand().click(Mexico);
	        getCommand().waitFor(3);
	        log("Clicking on search",LogType.STEP);            
	        getCommand().isTargetVisible(Search);
		   	getCommand().click(Search);
		   	getCommand().waitFor(2);
	        getCommand().sendKeys(Search,Text);
	        			
            
		    getCommand().waitForTargetPresent(SearchButton);
		    if (getCommand().isTargetPresent(SearchButton))
		        {
		           log("Passing sku 'K-T949-4-CP' to the search box",LogType.STEP);
		           log("Clicking on search button",LogType.STEP);
			       getCommand().click(SearchButton);
			                    
			       getCommand().waitFor(5);   
		         }
	            
	            String currentPageUrl=getCommand().driver.getCurrentUrl();
	            		
	            if(currentPageUrl.contains("skuid=K-T949-4-CP"))
	            	{
	            		log("Product K-T949-4-CP is displayed",LogType.STEP);
	            	}
	            else
	            	{
	                 	log("Product K-T949-4-CP is not displayed",LogType.ERROR_MESSAGE);
	                 	Assert.fail("Product K-T949-4-CP is not displayed");
	                }   
	            
	            String DiscontinuedTextMexico=getCommand().getText(DiscontinuedMexico);
	     	    if(DiscontinuedTextMexico.contains("Este producto ha sido descontinuado."))
	     	  	 {
	             	log("Discontinued template displays",LogType.STEP);
	              }
	            else
	              {
	             	log("Discontinued template is not displayed",LogType.ERROR_MESSAGE);
	             	Assert.fail("Discontinued template is not displayed");
	              }
	       
	    }

	   catch(Exception ex)
       {
	           Assert.fail(ex.getMessage());
       }
	  return this;	
	 }
	 
	//Verify Literature Page
	public Hippo_LatinAmerica verifyLiteratureFromFooter()
	 {
		try
		{
			log("Clicking on Literature from Footer",LogType.STEP);
			
			String Linktext=getCommand().getText(Literature_Footer);			
			getCommand().click(Literature_Footer);
			getCommand().waitFor(5);
			log("Navigating to Literature page",LogType.STEP);			
			String currentPageUrl=getCommand().driver.getCurrentUrl();
			
			if(currentPageUrl.contains("literatura"))
			{
				log("clicking on the link "+Linktext+" is redirecting to Literature page",LogType.STEP);
			}
			else
			{
				log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
				Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
			} 
		
			List<WebElement> promoTiles=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div/section/div/div"));
			 
			
			for(WebElement tilesHeader:promoTiles)
			{		
			   String TilesText=tilesHeader.getText();
				 
			   if(TilesText.contains("Urinales secos Steward") || TilesText.contains("2015 Toilet Service Parts.pdf"))
				 {
							 
					log("Tiles '"+TilesText+"'are Displayed",LogType.STEP);
					getCommand().click(Urinales_module);
					log("Clicked on Urinales secos Steward Module",LogType.STEP);
					getCommand().waitFor(10);
					String Url=getCommand().driver.getCurrentUrl();
					if(Url.contains("pdf"))
					{
						log("pdf document is displayed",LogType.STEP);
						getCommand().goBack();
						getCommand().waitFor(5);
						getCommand().goBack();
						getCommand().waitFor(5);
						log("Navigating to Main page",LogType.STEP);
					}
					else
					{
						log("pdf document is not displayed",LogType.ERROR_MESSAGE);
						 Assert.fail("pdf document is not displayed");
					}
				 }
				 else
				 {
				     log("Modules are not displayed in Literature page",LogType.ERROR_MESSAGE);
				     Assert.fail("Modules are not displayed in Literature page");
				 }		  
		}
		}		
		catch(Exception ex)
        {
            Assert.fail(ex.getMessage());
        }
			
			return this;
		}
   	 
	//verify Literature From Bathroom
	public Hippo_LatinAmerica verifyLiteratureFromBathroom()
	 {
	   try
	   {
		   log("Clicking on Bathroom in Global navigation",LogType.STEP);
				   	      
	   	    getCommand().click(Bathroom);
	        getCommand().waitFor(5);
	        String Linktext=getCommand().getText(Literature_Bathroom);
	        log("Clicking on Literature from Bathroom",LogType.STEP);
	        getCommand().click(Literature_Bathroom);
	        getCommand().waitFor(5);
			
			 log("Navigating to Literature page",LogType.STEP);
			 
			 String currentPageUrl=getCommand().driver.getCurrentUrl();
				
				if(currentPageUrl.contains("literatura"))
				{
					log("clicking on the link "+Linktext+" is redirecting to Literature page",LogType.STEP);
				}
				else
				{
					log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
					Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
				} 
				
		
		List<WebElement> promoTiles=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div/section/div/div"));
			 
		
		for(WebElement tilesHeader:promoTiles)
		{		
		   String TilesHeadersText=tilesHeader.getText();
			 
		   if(TilesHeadersText.contains("Urinales secos Steward") || TilesHeadersText.contains("2015 Toilet Service Parts.pdf"))
			 {
						 
				log("Tiles '"+TilesHeadersText+"'are Displayed",LogType.STEP);
				getCommand().click(Urinales_module);
				log("Clicked on Urinales Module",LogType.STEP);
				getCommand().waitFor(10);
				String Url=getCommand().driver.getCurrentUrl();
				if(Url.contains("pdf"))
				{
					log("pdf document is displayed",LogType.STEP);
					getCommand().goBack();
					log("Navigating to Main page",LogType.STEP);
				}
				else
				{
					log("pdf document is not displayed",LogType.ERROR_MESSAGE);
					 Assert.fail("pdf document is not displayed");
				}
			 }
			 else
			 {
			     log("Modules are not displayed in Literature page",LogType.ERROR_MESSAGE);
			     Assert.fail("Modules are not displayed in Literature page");
			 }		 
	      }
	   }
	   catch(Exception ex)
	   {
		   ex.getMessage();
	   }
		return this;
	 }
 	 
	//Verify Where To Buy page
	public Hippo_LatinAmerica verifyWhereToBuy()
	 {
	  try
	   {
		 log("clicking on Where To Buy link",LogType.STEP);
		 getCommand().click(WhereToBuyLink);
		 getCommand().waitFor(5);
		 log("Where To Buy page displays",LogType.STEP);				 
 		 String currentPageUrl=getCommand().getDriver().getCurrentUrl();
 		 if(currentPageUrl.contains("dondecomprarlatinamerica"))
 	   	{
 			log("URL '" +currentPageUrl+"' is displayed",LogType.STEP);
 		}
 		else
 		{
 		  log("Where To Buy Page is not displayed",LogType.ERROR_MESSAGE);
 		  Assert.fail("Where To Buy Page is not displayed");
 		}
	  }catch(Exception ex)
	  {
		  Assert.fail(ex.getMessage());
	  }
 		 return this;
	 }

	//Verify Footer Layout 
	public Hippo_LatinAmerica verifyFooterLayout()
     {
		 try
	     {
		getCommand().click(English);
		getCommand().waitFor(5);
		List<WebElement> FooterHeader=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[1]/div/div[2]/ul/li/span/span"));
        int NoOfFooterHeaders=FooterHeader.size();
        
        log("Verify Footer Layout",LogType.STEP);
       
    
      if(NoOfFooterHeaders==5)       
         {
               log("Footer is composed of 5 columns",LogType.STEP);          
         }
      else
        {
          log("Footer is not composed of 5 columns",LogType.ERROR_MESSAGE);
          Assert.fail("Footer is not composed of 5 columns");         
        }
      
      for(int i=0;i<NoOfFooterHeaders;i++)

      {

        String HeaderText = FooterHeader.get(i).getText();
        if(HeaderText.equalsIgnoreCase("OUR COMPANY")|| HeaderText.equalsIgnoreCase("KOHLER CO.")|| HeaderText.equalsIgnoreCase("HELP")|| HeaderText.equalsIgnoreCase("TRADE")|| HeaderText.equalsIgnoreCase("SOCIAL"))
        {
              log("Expected Column text: "+HeaderText+", is displayed under Footer section",LogType.STEP);
        }

       else
        {
              log("Expected Column text:" +HeaderText+" is not displayed under Footer section",LogType.ERROR_MESSAGE);
              Assert.fail("Expected Column text: " +HeaderText+"is not displayed under Footer section");
        }
      }
   
    log("Checking the navigation of each link under Footer section",LogType.STEP);
    List<WebElement> FooterHeaders=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div/div/div[2]/ul/li/span/span"));

    for (int j = 1; j <= FooterHeaders.size(); j++)

    {
        String pageTitle = getCommand().driver.getTitle();
        List<WebElement> footerheaderLinks = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[1]/div/div[2]/ul/li["+j+"]/ul/li/a"));

        for(int i=0;i<=footerheaderLinks.size();i++)
        {
            List<WebElement> footerheaderlinks = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[1]/div/div[2]/ul/li["+j+"]/ul/li/a"));


        	String Linktext= footerheaderlinks.get(i).getText();
           log("Clicking on the link "+Linktext,LogType.STEP);
          
           JS.executeScript("arguments[0].scrollIntoView(true);", footerheaderlinks.get(i));
           
         
         
//         String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN);
//          footerheaderlinks.get(i).sendKeys(selectLinkOpeninNewTab);
           footerheaderlinks.get(i).click();
        	   
          
          getCommand().waitFor(3);

          ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
          
          if(listofTabs.size()==1)
          {
        	  String CurrentpageTitle = getCommand().driver.getTitle();
              if(pageTitle.equals(CurrentpageTitle))
               {
                     log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
                     Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
               }

              else
                 {                                             
                      log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
                 }
              getCommand().goBack();
              getCommand().waitFor(5);
          }
          else
          {
          log("Switching to new tab",LogType.STEP);
          if(browserName.equals("safari"))
          {
        	
        	  
            	  getCommand().driver.switchTo().window(listofTabs.get(0));

        	 
          }
          else
          {
          getCommand().driver.switchTo().window(listofTabs.get(1));
          }
          getCommand().waitFor(4);
          String CurrentpageTitle = getCommand().driver.getTitle();
         if(pageTitle.equals(CurrentpageTitle))
          {
                log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
                Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
          }

         else
            {                                             
                 log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
            }

          getCommand().driver.close();
          if(browserName.equals("safari"))
          {

          getCommand().driver.switchTo().window(listofTabs.get(1));
          }
        }
       }
      }
	     }               
   catch (Exception e) {
         Assert.fail(e.getMessage());
   }
	
      return this;
     }

	//Verify Kohler World Wide Banner
	public Hippo_LatinAmerica verifyKohlerWorldWide()
	 {
        
     try
     {
    	getCommand().waitFor(5);
		getCommand().click(English);
		getCommand().waitFor(2);
		getCommand().click(KohlerWorldwide);
		List<WebElement> ColumnsHeader=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[2]/div[1]/ul/li/span"));
        int NoOfColumnsCount=ColumnsHeader.size();
     
      if(NoOfColumnsCount==6)       
         {
               log("Kohler World Wide banner is composed of 6 columns",LogType.STEP);          
         }
      else
        {
          log("Kohler World Wide banner is not composed of 6 columns",LogType.ERROR_MESSAGE);
          Assert.fail("Kohler World Wide banner is not composed of 6 columns");         
        }
    
      for(int i=0;i<NoOfColumnsCount;i++)

      {
    	  
        getCommand().waitFor(5);
        String ColumnsHeaderText = ColumnsHeader.get(i).getText();
        if(ColumnsHeaderText.equalsIgnoreCase("North America")|| ColumnsHeaderText.equalsIgnoreCase("South America")|| ColumnsHeaderText.equalsIgnoreCase("Africa")|| ColumnsHeaderText.equalsIgnoreCase("Europe")||
        		ColumnsHeaderText.equalsIgnoreCase("Asia")||ColumnsHeaderText.equalsIgnoreCase("Oceania"))
        {
              log("Expected Column text: "+ColumnsHeaderText+", is displayed in World Wide banner",LogType.STEP);
        }

       else
        {
              log("Expected Column text :" +ColumnsHeaderText+" is not displayed in World Wide banner",LogType.ERROR_MESSAGE);
              Assert.fail("Expected Column text :" +ColumnsHeaderText+"  is not displayed in World Wide banner");
        }
      }   
     
      
      log("Checking the navigation of each link",LogType.STEP);
      List<WebElement> BannerHeaders=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[2]/div[1]/ul/li/span"));

      for (int j = 1; j <= BannerHeaders.size(); j++)

      {
    	  
          String Url = getCommand().driver.getCurrentUrl();         
          List<WebElement> bannerheaderlinks = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[2]/div[1]/ul/li["+j+"]/ul/li/a"));
          for(WebElement bannerheaderlink: bannerheaderlinks)
          {
             String Linktext = bannerheaderlink.getText();
             log("Clicking on the link "+Linktext,LogType.STEP);
            if(browserName.equals("safari"))
            {
            	 String selectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN);          
                 bannerheaderlink.sendKeys(selectLinkOpeninNewTab);
            }
            else
            {
            	 String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);          
                 bannerheaderlink.sendKeys(selectLinkOpeninNewTab);
            }
           
            getCommand().waitFor(5);

            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());

        if(listofTabs.size()>1)
          {
            
            log("Switching to new tab,LogType.STEP",LogType.STEP);
            
            if(browserName.equals("safari"))
            {
                getCommand().driver.switchTo().window(listofTabs.get(0));

            }
            else
            {
                getCommand().driver.switchTo().window(listofTabs.get(1));

            }
            getCommand().waitFor(4);
            String CurrentpageUrl = getCommand().driver.getCurrentUrl();
           if(Url.equals(CurrentpageUrl))
           
            {
                  log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
                  Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
            }

           else
              {                                             
                   log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
                   
              }

            getCommand().driver.close();
            
            if(browserName.equals("safari"))
            {
                getCommand().driver.switchTo().window(listofTabs.get(1));

            }else {
                getCommand().driver.switchTo().window(listofTabs.get(0));

            }
          }
        
        else
        {
        	String CurrentpageUrl = getCommand().driver.getCurrentUrl();
            if(Url.equals(CurrentpageUrl))
             {                        
                 log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
                 Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
             }
             else
             {                        
                 log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
             }
            
            getCommand().goBack();
            getCommand().waitFor(5);
            WebElement Worldwide=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[1]/ul[2]/li[1]/a"));
            
           Worldwide.click();
           

        }      
      }
      } 
      log("Verifying navigation of each other Kohler brands",LogType.STEP);
  
      List<WebElement> Brands = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[2]/div[2]/ul/li/a"));	
		
      int NoofOtherBrands=Brands.size();
      
      if(NoofOtherBrands==6)       
       {
             log("6 other kohler brands are present",LogType.STEP);          
       }
     else
      {
        log("Other Kohler brands are not present",LogType.ERROR_MESSAGE);
        Assert.fail("Other Kohler brands are not present");         
      }
      for(WebElement Brand:Brands)
		{
    	    String Url1 = getCommand().driver.getCurrentUrl();
    	  
			String href = Brand.getAttribute("href");
			log("Clicking on the brand with link: "+href,LogType.STEP);
			if(browserName.equals("safari"))
			{
				String SelectLinkOpeninNewTab = Keys.chord(Keys.COMMAND,Keys.RETURN);
				Brand.sendKeys(SelectLinkOpeninNewTab);
			}
			else
			{
				String SelectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
				Brand.sendKeys(SelectLinkOpeninNewTab);
			}
			
			
			getCommand().waitFor(9);
			ArrayList<String> ListOfTabs = new ArrayList<String>(getCommand().driver.getWindowHandles());
			if(browserName.equals("safari"))
			{
				getCommand().driver.switchTo().window(ListOfTabs.get(0));

			}
			else
			{
				getCommand().driver.switchTo().window(ListOfTabs.get(1));

			}
			
			String CurrentPageTitle = getCommand().driver.getTitle();
			log("Navigated to brand '"+CurrentPageTitle+"'",LogType.STEP);
  
			String CurrentpageUrl = getCommand().driver.getCurrentUrl();
	           if(Url1.equals(CurrentpageUrl))
	           
	            {
	                  log("Clicking on the brand:  "+CurrentPageTitle+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
	                  Assert.fail("Clicking on the brand: "+CurrentPageTitle+" is not redirecting to the corresponding page");
	            }

	           else
	              {                                             
	                   log("Clicking on the brand: "+CurrentPageTitle+" is redirecting to the corresponding page",LogType.STEP);
	              }
			
		
				getCommand().driver.close();
				if(browserName.equals("safari"))
				{
					getCommand().driver.switchTo().window(ListOfTabs.get(1));

				}
				else
				{
		    		getCommand().driver.switchTo().window(ListOfTabs.get(0));				

				}
			}
		}
       
      catch(Exception ex)
      {
          Assert.fail(ex.getMessage());
      }
     
		 return this;
	 }
     
	//Verify Type Ahead Suggestions
	public Hippo_LatinAmerica verifyTypeAheadSpanish(String keyword)
	{
	 try
	  {
        LatamxSearchData search = LatamxSearchData.fetch(keyword);
		
		String Text = search.keyword;
		log("Entering 'ino' in searchbox",LogType.STEP);
		getCommand().isTargetVisible(Search);
		
		getCommand().click(Search);
		getCommand().waitFor(2);
		getCommand().sendKeys(Search, Text);
		
		log("Display of Search suggestions ino",LogType.STEP);
				
		
		   List<WebElement> suggestions = getCommand().driver.findElements(By.xpath("(//*[@id='search-suggestions-container']/div/ul)[2]"));		   
		
		    for(WebElement SuggestionsList:suggestions)
		    	
		   {  	 
		        getCommand().waitFor(5);
		        String SuggestionsText=SuggestionsList.getText();
		    				    			    		
		    	if(SuggestionsText.contains("Inodoros") || SuggestionsText.contains("Inodoros Institucionales") || SuggestionsText.contains("Asientos de inodoro")) 
		    	  {
		    		 log("Type Ahead suggestions '" +SuggestionsText+ "' are displayed",LogType.STEP);
				  }
		    		    	
		    	else
				  {
				    log("Type Ahead suggestions not displayed",LogType.ERROR_MESSAGE);
				    Assert.fail("Type Ahead suggestions are not displayed");
				  }
		   }
	     }catch(Exception ex)
          {
              Assert.fail(ex.getMessage());
          }
   return this;
	    
	}		
	
	//verify Type Ahead English
    public Hippo_LatinAmerica verifyTypeAheadEnglish(String keyword)
	{
     try
      { 
		log("Entering 'sin' in searchbox",LogType.STEP);
		LatamxSearchData search = LatamxSearchData.fetch(keyword);
			
		String Text = search.keyword;
		getCommand().click(English);
		getCommand().waitFor(3);
		getCommand().isTargetVisible(Search);		
		getCommand().click(Search);
		getCommand().sendKeys(Search, Text);
		
		log("Display of Search suggestions for sin",LogType.STEP);
				
			    
	   List<WebElement> suggestions = getCommand().getDriver().findElements(By.xpath("(//*[@id='search-suggestions-container']/div/ul)[2]"));		   
	    
	   for(WebElement SuggestionsList:suggestions)
	    {
		   getCommand().waitFor(5);
		   String SuggestionsText=SuggestionsList.getText();
   		     		
   		if(SuggestionsText.contains("Singulier") || SuggestionsText.contains("Kitchen Sinks") || SuggestionsText.contains("Bathroom Sink Faucets")
   				|| SuggestionsText.contains("Bathroom Sinks")) 
   		  {
   		     log("Type Ahead suggestions '" +SuggestionsText+ "' are displayed",LogType.STEP);
		  }
   		    	
        else
		  {
		    log("Type Ahead suggestions not displayed",LogType.ERROR_MESSAGE);
		    Assert.fail("Type Ahead suggestions are not displayed");
		  }
        }
      }  
     catch(Exception ex)
        {
          Assert.fail(ex.getMessage());
        }
	   
		return this;
	}
    	
    //Verify Product details Page
    public Hippo_LatinAmerica verifyPDPPage(String keyword)
	{

       //verify Breadcrumbs
    try
    {
    	
    	getCommand().click(English);
		getCommand().waitFor(3);
		LatamxSearchData search = LatamxSearchData.fetch(keyword);
		String Text = search.keyword;
    		
		log("Passing sku: 8325-CS6 to the Search field",LogType.STEP);
		getCommand().isTargetVisible(Search);
    	getCommand().click(Search);
    	getCommand().sendKeys(Search, Text);
 			            
        getCommand().waitForTargetPresent(SearchButton);
        if (getCommand().isTargetPresent(SearchButton))
        {
        	log("Clicked on search button",LogType.STEP);
	        getCommand().click(SearchButton);
	        getCommand().waitFor(10);      		
       	 }      
        
       //verify product name
        
        String productText= getCommand().getText(ProductName);
        if(productText.contains("Kensho"))
        {
     		log("Product 'Kensho' is displayed",LogType.STEP);
     	}
	    else
    	{
     		log("Product 'Kensho' is not displayed",LogType.ERROR_MESSAGE);
     		Assert.fail("Product Kensho is not displayed");
     	}
      
        //verify product sku
        
        String SKU= getCommand().getText(PDPProductSKU);
        if(SKU.contains("K-8325-CS6"))
        {
     		log("SKU K-8325-CS6 is displayed",LogType.STEP);
     	}
	    else
    	{
     		log("SKU K-8325-CS6 is not displayed",LogType.ERROR_MESSAGE);
     		Assert.fail("SKU K-8325-CS6 is not displayed");
     	}	
        
      //verify Store locator 
       if(getCommand().isTargetVisible(StoreLocator))
       {
    		log("Store Locator is displayed",LogType.STEP);
       }
	   else
   	   {
    		log("Store Locator is not displayed",LogType.ERROR_MESSAGE);
    		Assert.fail("Store Locator is not displayed");
       }	
       
      //verify compare
           
       if(getCommand().isTargetVisible(Compare))
       {
   		log("Compare is displayed",LogType.STEP);
       }
	   else
  	   {
   		log("Compare is not displayed",LogType.ERROR_MESSAGE);
   		Assert.fail("Compare is not displayed");
       }	
      
       
    //verify Share button
    
    if(getCommand().isTargetVisible(ShareButton))
    {
 		log("Share button is displayed is displayed",LogType.STEP);
 	}
    else
	{
 		log("Share button is displayed is not displayed",LogType.ERROR_MESSAGE);
 		Assert.fail("Share button is displayed is not displayed");
 	}
   
    //verify print button
    
    if(getCommand().isTargetVisible(Print))
    {
 		log("Print button is displayed is displayed",LogType.STEP);
 	}
    else
	{
 		log("Print button is displayed is not displayed",LogType.ERROR_MESSAGE);
 		Assert.fail("Print button is displayed is not displayed");
 	}
    
    	
	   List<WebElement> Breadcrumbs = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/ul/li"));		   
	   for (int j = 1; j <= Breadcrumbs.size(); j++)
	    {

	    List<WebElement> BreadCrumbsLists = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/ul/li["+j+"]"));   
	    for(WebElement BreadcrumbsList:BreadCrumbsLists)
	    	
	   {  	 
	    		String BreadcrumbsText=BreadcrumbsList.getText();
	    		
	    		
	    		if(BreadcrumbsText.contains("Home") || BreadcrumbsText.contains("Bathroom") || BreadcrumbsText.contains("Bathroom Sinks")) 
	    		{
	    			log("Bead Crumbs' "+BreadcrumbsText+"'  is displayed in the page",LogType.STEP);
			    }
	    		else
	    		{
	    			log("Breadcrumbs not displayed",LogType.ERROR_MESSAGE); 
	    			Assert.fail("Breadcrumbs not displayed");
	    		}
	         }	    
	      }
	   
	 //verify Image  
       
       if(getCommand().isTargetVisible(PDPImage))
          {
             log("Product Image is dispalyed",LogType.STEP);
          }
           
      else
          {
               log("Product Image is not dispalyed",LogType.ERROR_MESSAGE);
               Assert.fail("Product Image is not dispalyed");
          }

    //verify Thumbnails
       
       if(getCommand().isTargetVisible(Thumbnails))
       {
              log("ThumbNails display on the main page",LogType.STEP);
       }
       else
       {
              log("ThumbNails doesn't display on the main page",LogType.ERROR_MESSAGE);
              Assert.fail("ThumbNails doesn't display on the main page");
       }

       
       
	//Verify Features
       String browserName = caps.getBrowserName();

       
      getCommand().waitFor(3);
      String ProductFeatures =getCommand().getText(Features);
      
      if(browserName.equals("MicrosoftEdge") || browserName.equals("safari"))
      {
      
   	   if(ProductFeatures.trim().equalsIgnoreCase("FEATURES"))
          {
       		log("Product feature is displayed",LogType.STEP);
          }
   	   else
   	   {
   		   log("Product feature is not displayed",LogType.ERROR_MESSAGE);
      		Assert.fail("Product feature is not displayed"); 
   	   }
      }
      
      else
      {
   	   
          if(ProductFeatures.trim().equals("FEATURE"))
        {
   		log("Product feature is displayed",LogType.STEP);
        }
	    else
  	     {
   		log("Product feature is not displayed",LogType.ERROR_MESSAGE);
   		Assert.fail("Product feature is not displayed");
        } 
      }    
    //Verify Service and Support
      
      String ProductServiceAndSupport =getCommand().getText(ServiceSupport);
      
      
      if(browserName.equals("MicrosoftEdge") || browserName.equals("safari"))
      {
      
   	   if(ProductServiceAndSupport.equalsIgnoreCase("Service & Support"))
          {
       		log("Service & Support is displayed is displayed",LogType.STEP);
          }
   	   else
   	   {
   		   log("Service & Support is displayed is not displayed",LogType.ERROR_MESSAGE);
      		   Assert.fail("Service & Support is displayed is not displayed"); 
   	   }
      }
      
      else
      {
   	   
        if(ProductServiceAndSupport.equals("SERVICE & SUPPORT"))
        {
       	   log("Service & Support is displayed is displayed",LogType.STEP);
        }
 	    else
 	     {
 		   log("Service & Support is displayed is not displayed",LogType.ERROR_MESSAGE);
    	   Assert.fail("Service & Support is displayed is not displayed"); 
 	     }
      }    

      
    //Technical information
      
     
      String ProductTechnicalInformation =getCommand().getText(TechnicalInformation);
      
      
      if(browserName.equals("MicrosoftEdge") || browserName.equals("safari"))
      {
      
   	  if(ProductTechnicalInformation.equalsIgnoreCase("Technical Information"))
   	   {
      		log("Technical Information is displayed",LogType.STEP);
          }
  	    else
     	   {
      		log("Technical Information is not displayed",LogType.ERROR_MESSAGE);
      		Assert.fail("Technical Information is not displayed");
          }   
      }
      
      else
      {
   	   
        if(ProductTechnicalInformation.equals("TECHNICAL INFORMATION"))
        {
    		log("Technical Information is displayed",LogType.STEP);
        }
	    else
   	 {
    		log("Technical Information is not displayed",LogType.ERROR_MESSAGE);
    		Assert.fail("Technical Information is not displayed");
        }   
      }    

   //Similar products
		
    List<WebElement> columns = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[5]"));		   

    int count = columns.size();
    if(count>0)
    {
		log("4 similar products are displayed",LogType.STEP);
    }
	else
	{
		log("4 similar products are not displayed",LogType.ERROR_MESSAGE);
		Assert.fail("4 similar products are not displayed");
   }   
       }
     catch(Exception ex)
     {
    	ex.getMessage();
     }

		   return this;
	}
		
    //Verify Kitchen Uber Category Page
    public Hippo_LatinAmerica verifyUberCategoryPage()
	{
		getCommand().click(English);
		getCommand().waitFor(3);
		getCommand().click(Kitchen);
		getCommand().waitFor(5);
		getCommand().click(KitchenUberCategory);
		getCommand().waitFor(10);
		
		log("Kitchen uber category page is displayed",LogType.STEP);
				
		List<WebElement> Category= getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[1]/div[2]/div/ul"));
		int count=0;
		
	  for(WebElement list: Category) 
	  {
	    String CategoryText=list.getText();
		count=Category.size();
		if(count>0)
		{
			log("3 categories'"+CategoryText+"' are displayed under Category list" ,LogType.STEP);
		}
		
	    else
		{
		    log(" 0 categories are displayed in category list",LogType.ERROR_MESSAGE);
		    Assert.fail(" 0 categories are displayed in category list");
	    }
								
	 }
	  log("Verify Each product module has an image, Description and SKU#",LogType.STEP);
       
	    for(int k=2;k<=4;k++)
	  
        {           
              boolean images=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+k+"]/div/div[1]/a/div/img")).isDisplayed();
              boolean ProductDescription=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+k+"]/div/div[1]/a/span[1]")).isDisplayed();
              boolean sku=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+k+"]/div/div[1]/a/span[2]")).isDisplayed(); 
           
              Actions action = new Actions(getCommand().driver);
              WebElement ele = getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+k+"]/div/div[2]/div"));
              getCommand().waitFor(5);
              action.moveToElement(ele).build().perform();
              
               boolean quickView=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+k+"]/div/div[2]/div/button/span")).isDisplayed();
              
              
              if(images && sku && ProductDescription && quickView  ==true)
              {

                     log("Image,ProductDescription, SKU# and Quick view are displayed for " +k+ " module" ,LogType.STEP);
              }
              else
              {
                     log(k+ "Module is not displayed",LogType.ERROR_MESSAGE);
                     Assert.fail(k+ "Module is not displayed");
              }        
        }
        for(int l=6;l<=8;l++)
        {
        	 
             boolean images=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+l+"]/div/div[1]/a/div/img")).isDisplayed();
             boolean ProductDescription=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+l+"]/div/div[1]/a/span[1]")).isDisplayed();
             boolean sku=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+l+"]/div/div[1]/a/span[2]")).isDisplayed(); 
          
             Actions action = new Actions(getCommand().driver);
             WebElement ele2 = getCommand().driver.findElement(By.xpath("/html/body/div[1]/div/div/section/div[2]/div["+l+"]/div/div[2]/div"));
             JS.executeScript("arguments[0].scrollIntoView(true);", ele2);
             getCommand().waitFor(5);
             action.moveToElement(ele2).build().perform();
             
              boolean quickView=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+l+"]/div/div[2]/div/button/span")).isDisplayed();
             
             
             if(images && sku && ProductDescription && quickView  ==true)
             {

                    log("Image,ProductDescription, SKU# and Quick view is displayed for " +l+ " module",LogType.STEP);
             }
             else
             {
                    log(l+ "module is not displayed",LogType.ERROR_MESSAGE);
                    Assert.fail(l+ "module is not displayed");
             }   
        }
        
        for(int m=10;m<=12;m++)
        {
              
              boolean images=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+m+"]/div/div[1]/a/div/img")).isDisplayed();
              boolean ProductDescription=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+m+"]/div/div[1]/a/span[1]")).isDisplayed();
              boolean sku=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+m+"]/div/div[1]/a/span[2]")).isDisplayed(); 
           
              Actions action = new Actions(getCommand().driver);
              WebElement ele3 = getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+m+"]/div/div[2]/div"));
              JS.executeScript("arguments[0].scrollIntoView(true);", ele3);
              getCommand().waitFor(5);
              action.moveToElement(ele3).build().perform();
              
              boolean quickView=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[2]/div["+m+"]/div/div[2]/div/button/span")).isDisplayed();
              
              
              if(images && sku && ProductDescription && quickView  ==true)
              {

                  log("Image,ProductDescription, SKU# and Quick view are displayed for " +m+ " module",LogType.STEP);
              }
              else
              {
                     log(m+ "module is not displayed",LogType.ERROR_MESSAGE);
                     Assert.fail(m+ "module is not displayed");
              }        
        }
     
		return this;
	
    }
  
    //Verify Hero/Image carousel functionality
    public Hippo_LatinAmerica verifyHeroImageCrousel()
   {
       try
       {
              List<String> Imagetext_NavDots = new ArrayList<String>();
              List<String> Imagetext_NextArrow = new ArrayList<String>();
              List<String> Imagetext_PrevArrow = new ArrayList<String>();

              List<WebElement> NavDots = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/ul/li"));
              
              int i=2;
              int j=1;
              int l=2;
              int n=6;
              
              log("Clicking on each nav dots and checking image changes for each nav dot click",LogType.STEP);
              
              for(WebElement NavDot : NavDots)
              {
                    
                    log("Clicking on "+j+" Nav dot",LogType.STEP);
                    
                    NavDot.click();
                    
                    getCommand().waitFor(3);
                                         
                    WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+i+"]/div[2]/span/span[@class='koh-banner-title']"));
                    
                    if(Image.isDisplayed())
                    {
                           log("Getting image title and adding to list",LogType.STEP);
                           String ImageText = Image.getText();
                           Imagetext_NavDots.add(ImageText);                           
                           i++;                       
                    }
                    
                    else
                    {
                           log("Image is not displayed after clicking on "+j+" Nav dot",LogType.ERROR_MESSAGE);
                           Assert.fail("Image is not displayed after clicking on "+j+" Nav dot");
                    }
                    j++;
           }
              
              log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
              
              Assert.assertTrue(CompareDataFromSameList(Imagetext_NavDots),"Clicking on each navigation dots, image not changes");
              
              log("Clicking on each navigation dots, image changes",LogType.STEP);
              
              log("Checking Scroll feature using next arrow",LogType.STEP);
              
              for(int k=1;k<=NavDots.size();k++)
              {
                    log("Clicking on next arrow and checking image changes",LogType.STEP);
                    
                   getCommand().click(HeroNextArrow);
                    
                    getCommand().waitFor(3);
                    
                    WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+l+"]/div[2]/span/span[@class='koh-banner-title']"));
                    
                    if(Image.isDisplayed())
                    {
                           log("Getting image title and adding to list",LogType.STEP);
                           String ImageText = Image.getText();
                           Imagetext_NextArrow.add(ImageText);                         
                           l++;                       
                    }
                    
                    else
                    {
                           log("Image is not displayed after clicking on next arrow",LogType.ERROR_MESSAGE);
                           Assert.fail("Image is not displayed after clicking on next arrow");
                    }
              }
              
            log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
              
             Assert.assertTrue(CompareDataFromSameList(Imagetext_NextArrow),"Clicking on next arrow, image not changes and scroll is not working");
              
              log("Clicking on next arrow, image changes and scroll is working",LogType.STEP);
              
             //getCommand().mouseHover(HeroNextArrow).click(HeroNextArrow);
              
              log("Checking Scroll feature using previous arrow",LogType.STEP);
              
              getCommand().click(HeroNextArrow);
              getCommand().waitFor(5);

              for(int m=1;m<=NavDots.size();m++)
              {
                    log("Clicking on prev arrow and checking image changes",LogType.STEP);
                    
                    getCommand().click(HeroPrevArrow);
                    
                    getCommand().waitFor(3);
                    
                    WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+n+"]/div[2]/span/span[@class='koh-banner-title']"));
                    
                    if(Image.isDisplayed())
                    {
                           log("Getting image title and adding to list",LogType.STEP);
                           String ImageText = Image.getText();
                           Imagetext_PrevArrow.add(ImageText);                         
                           n--;                       
                    }
                    
                    else
                    {
                           log("Image is not displayed after clicking on prev arrow",LogType.ERROR_MESSAGE);
                           Assert.fail("Image is not displayed after clicking on prev arrow");
                    }
              }
              
           log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
              
              Assert.assertTrue(CompareDataFromSameList(Imagetext_PrevArrow),"Clicking on prev arrow, image not changes and scroll is not working");
              
              log("Clicking on prev arrow, image changes and scroll is working",LogType.STEP);
 
              
       }
       
       
       catch(Exception ex)
       {
              ex.getMessage();
              Assert.fail(ex.getMessage());
       }

       return this;
   }

    //verify Hero Image Navigation
    public Hippo_LatinAmerica verifyHeroImageNavigation() {
	
	 try
     {      
             List<WebElement> NavDots = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/ul/li"));
            
            int i=2;
            int j=1;
            
            log("Clicking on each nav dots and clicking on image displayed for each nav dot",LogType.STEP);
            
            for(int k=0;k<NavDots.size();k++)
            {
                  List<WebElement> Navdots = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/ul/li"));
                  
                  String PageUrl = getCommand().getPageUrl();
                  
                  log("Clicking on "+j+" Nav dot",LogType.STEP);
                  
                  Navdots.get(k).click();
                  
                  getCommand().waitFor(5);
                  
       WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+i+"]"));
                  
                  if(Image.isDisplayed())
                  {
                         String ImageText = Image.getText();    
                         
                         log("Clicking on image: "+ ImageText,LogType.STEP);
                         
                         Image.click();
                         
                         getCommand().waitFor(10);
                         
                         String CurrentPageUrl = getCommand().getPageUrl();
                         
                         Assert.assertNotEquals(PageUrl, CurrentPageUrl,"Clicking on image "+ ImageText+ " is not redirecting to its site");
                         
                         log("Clicking on image: '"+ ImageText+ "' is redirecting to its site: "+getCommand().getPageTitle(),LogType.STEP);
                         
                         getCommand().goBack();
                         
                         getCommand().waitFor(5);
                                              
                         i++;                       
                  }
                  
                  else
                  {
                         log("Failed to click on "+j+" Image",LogType.ERROR_MESSAGE);
                         Assert.fail("Failed to click on "+j+" Image");
                  }
                  j++;
         }
     }
     
     catch(Exception ex)
     {
            ex.getMessage();
            Assert.fail(ex.getMessage());
     }
     return this;

}

    //Verify Compare feature
    public Hippo_LatinAmerica  VerifyComparefeature() 
    {
    	try
        {
               log("Clicking on Bathroom and Navigating to Pedestal page",LogType.STEP);
               getCommand().waitFor(5);
               getCommand().driver.findElement(By.xpath("(//a[contains(text(),'EN')])[1]")).click();
               getCommand().waitFor(5);
               getCommand().driver.findElement(By.xpath("(//span[contains(text(),'Bathroom')])[1]")).click();
               getCommand().waitFor(5);
               getCommand().driver.findElement(By.xpath("(//a[contains(text(),'Bathroom SInks')])[1]")).click();
               getCommand().waitFor(5);
               String Parent=getCommand().driver.getWindowHandle();
               log("Listing all the products present in the page",LogType.STEP);
                 
            
               
               
               List<WebElement> products=getCommand().driver.findElements(By.xpath("//div[@class='koh-search-results']/div"));
               for(int i=0;i<3;i++)
               {
            	   if(browserName.equals("chrome")||browserName.equals("firefox")||browserName.equals("edge"))
            	   {
                	   getCommand().driver.switchTo().window(Parent);    

            	   }
                     WebElement element=products.get(i);
                                    
                     Actions sact=new Actions(getCommand().driver);
                     sact.moveToElement(element).build().perform();
                     
                     getCommand().waitFor(5);
                     log("Click on the compare link in the product",LogType.STEP);
                     List<WebElement> Compare=getCommand().driver.findElements(By.xpath("//a[contains(text(),'+Compare')]"));
                     getCommand().waitFor(3);
                     Compare.get(i).click();
                     
                     getCommand().waitFor(2);
                     Set<String> s1=getCommand().driver.getWindowHandles();
                     Iterator<String> it=s1.iterator();
                     while(it.hasNext())
                     {
                            String ChildWindow=it.next();
                            getCommand().driver.switchTo().window(ChildWindow);
                            String browserName = caps.getBrowserName();
                            if(browserName.equals("firefox"))
                            {                                 
                                 getCommand().click(CompareOverlayArrow);
                            }
                            log("switching to compare products window",LogType.STEP);
                     }
                    
                     getCommand().waitFor(5);
               }

               String browserName = caps.getBrowserName();

               if(browserName.equals("Firefox"))
               {
            	   getCommand().click(CompareOverlayArrow);
               }
              
               log("Verify compare panel shows three product modules (with thumbnail, product name, SKU# and price) each with a close icon",LogType.STEP);
               List<WebElement> productModules = getCommand().driver.findElements(By.xpath("//div[@class='koh-product-tile-content']/a"));
               if(productModules.size()>=9)
               {
                     for(int l=1;l<=3;l++)
                     {
                            
                            getCommand().driver.findElement(By.xpath("//div[@class='koh-compare-items open']/div["+l+"]")).isDisplayed();
                            getCommand().driver.findElement(By.xpath("(//div[@class='koh-compare-item'])["+l+"]/a/img")).isDisplayed();
                            getCommand().driver.findElement(By.xpath("(//span[@class='koh-compare-name'])["+l+"]")).isDisplayed();
                            getCommand().driver.findElement(By.xpath("(//span[@class='koh-compare-sku'])["+l+"]")).isDisplayed();

                     }
                     log("Verify clear results link, compare CTA and expand arrow is displayed",LogType.STEP);
                     getCommand().driver.findElement(By.xpath("//button[contains(text(),'Clear Results')]")).isDisplayed();
                     getCommand().driver.findElement(By.xpath("(//a[contains(text(),'Compare')])[1]")).isDisplayed();
                     getCommand().driver.findElement(By.xpath("//div[@class='koh-compare-header']/button[1]")).isDisplayed();

               } 
               else
               {
                     log("Compare panel does not displays product modules",LogType.ERROR_MESSAGE);
                     Assert.fail("Compare panel does not displays product modules");
               }
               log("Click on Compare CTA link",LogType.STEP);
               getCommand().driver.findElement(By.xpath("//a[contains(text(),'Compare')][1]")).click();
               getCommand().waitFor(5);
               List<WebElement> productColumns =getCommand().driver.findElements(By.xpath("//ul[@class='koh-compare-features-list']"));
               Assert.assertEquals(4, productColumns.size(), "4 Columns are displayed in compare modal");             
               Assert.assertTrue(getCommand().driver.findElement(By.xpath("//button[@class='koh-compare-print']")).isDisplayed(), "Print icon is located in the page");
              
               log("Verify Each product column has an image, product name, SKU# and price, and product features",LogType.STEP);
               for(int k=2;k<=4;k++)
               {
                     
                     boolean images=getCommand().driver.findElement(By.xpath("(//div[@class='koh-compare-top'])["+k+"]/a/img")).isDisplayed();
                     boolean Productname=getCommand().driver.findElement(By.xpath("(//div[@class='koh-compare-top'])["+k+"]/a/span[1]")).isDisplayed();
                     boolean sku=getCommand().driver.findElement(By.xpath("(//div[@class='koh-compare-top'])["+k+"]/a/span[2]")).isDisplayed();
                     boolean price=getCommand().driver.findElement(By.xpath("(//div[@class='koh-compare-top'])["+k+"]/a/span[3]")).isDisplayed();
                     boolean Productfeatures=getCommand().driver.findElement(By.xpath("(//ul[@class='koh-compare-features-list'])["+k+"]")).isDisplayed();
                     boolean Print=getCommand().driver.findElement(By.xpath("/html/body/div[146]/div/section/div/div[2]/div[1]/div/button")).isDisplayed();
                     
                     if(images && sku && Productname  && price && Productfeatures && Print  ==true)
                     {

                            log("Each product column has an image, product name, SKU#,product features and Print",LogType.STEP);
                     }
                     else
                     {
                            log("Missing elements from product features",LogType.ERROR_MESSAGE);
                            Assert.fail("Missing elements from product features");
                     }
                     
                     
               }
               
               getCommand().click(close);
               List<WebElement> Compare_panel = getCommand().driver.findElements(By.xpath("//div[@id='comparePanel']/div"));
               getCommand().waitFor(5);
               log("Click on close results link and verifying compare panel disappears",LogType.STEP);
               getCommand().driver.findElement(By.xpath("//button[contains(text(),'Clear Results')]")).click();
               getCommand().waitFor(5);

               if(Compare_panel.size() != 0)
               {
                     log("Compare panel disappears",LogType.STEP);
               }            
               else
               {
                     log("Compare panel displayed",LogType.ERROR_MESSAGE);
                     Assert.fail("Compare panel displayed");
               }            
               getCommand().waitFor(10);
        }
        catch(Exception e)
        {
               Assert.fail(e.getMessage());
        }
        return this;      
        

    }

    //Verify home page layout
     public Hippo_LatinAmerica verify_HomePageLayout()
     {
    	 try
    	 {
         getCommand().waitFor(5);		 
         
         String WorldWideText = getCommand().getText(KohlerWorldWide);
         
         if(getCommand().isTargetVisible(KohlerWorldWide) && WorldWideText.trim().equals("KOHLER en el Mundo"))
         {
                log(WorldWideText+" is displayed",LogType.STEP);
         }
         
         else
         {
                log(WorldWideText+" is not displayed",LogType.ERROR_MESSAGE);
                Assert.fail(WorldWideText+" is not displayed");
         }
         
         WebElement Hero =  getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section"));
         
         String HeroText = Hero.getAttribute("class");
         
         if(Hero.isDisplayed() && HeroText.contains("hero"))
         {
                log("Hero is displayed",LogType.STEP);
         }
         
         else
         {
                log("Hero is not displayed",LogType.ERROR_MESSAGE);
                Assert.fail("Hero is not displayed");
         }
         
   WebElement PromoModules =  getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[2]/section"));
   
   String PromoModulesText = PromoModules.getAttribute("class");
         
         if(PromoModules.isDisplayed() && PromoModulesText.contains("promo-grid"))
         {
                log("PromoModules is displayed",LogType.STEP);
         }
         
         else
         {
                log("PromoModules is not displayed",LogType.ERROR_MESSAGE);
                Assert.fail("PromoModules is not displayed");
         }
         
   WebElement Footer = getCommand().driver.findElement(By.xpath("//*[@id=\"koh-page-outer\"]/div/footer/div[1]/div/div[2]"));
         
         String FooterText = Footer.getAttribute("class");
         
         if(Footer.isDisplayed() && FooterText.contains("footer"))
         {
                log("Footer is displayed",LogType.STEP);
         }
         
         else
         {
                log("Footer is not displayed",LogType.ERROR_MESSAGE);
                Assert.fail("Footer is not displayed");
         }
         
        
         
          if(getCommand().isTargetVisible(SearchBox))
                {
                      log("Search is displayed",LogType.STEP);
                }
                
                else
                {
                      log("Search is not displayed",LogType.ERROR_MESSAGE);
                      Assert.fail("Search is not displayed");
                }

         List<String> GlobalNav = new ArrayList<String>();          
         
         GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[1]/span");
         
         GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[2]/a");
         
         GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[3]/a");
         
         GlobalNav.add("//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[1]/nav/div[1]/a/img");

         for(String Xpath:GlobalNav)
         {                    
                WebElement Element = getCommand().driver.findElement(By.xpath(Xpath));
                String Text = Element.getText();
                if(Element.isDisplayed())
                {
                      if(!Text.isEmpty())
                      {
                    	  String browserName = caps.getBrowserName(); 
                    	  
                    	  if(browserName.equals("MicrosoftEdge") || browserName.equals("safari"))
                          {
                            if(Text.equalsIgnoreCase("Baños") || Text.equalsIgnoreCase("Cocina") || Text.equalsIgnoreCase("IDEAS")) 
                            {
                                   log(Text+" is displayed in Global Navigation",LogType.STEP);
                                   log("Correct Text: "+Text+" is displayed in Global Navigation",LogType.STEP);
                            }
                           else
                            {
                               log("Text: "+Text+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
                               Assert.fail("Text: "+Text+" is not displayed in Global Navigation");
                            }
                          }
                            else
                            {
                                if(Text.equals("BA�OS") || Text.equals("COCINA") || Text.equals("IDEAS")) 
                                {
                                   log(Text+" is displayed in Global Navigation",LogType.STEP);                                  
                                }
                              else
                                {
                                   log("Text: "+Text+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
                                   Assert.fail("Text: "+Text+" is not displayed in Global Navigation");
                                }
                           }
                      }
                      
                      else
                      {
                             log("Logo is displayed in Global Navigation",LogType.STEP);
                      }
                }
                
                else
                {
                      if(!Text.isEmpty()) {
                             log(Text+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
                      }
                      else {
                             log("Logo is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
                           }
                }
         }
  }
    	 catch(Exception ex) {
             Assert.fail(ex.getMessage());
             }
     	 return this;
     }
 	   
     //Method to verify data from same list   
     public boolean CompareDataFromSameList(List<String> list)
    		{
    			for (int i = 0; i < list.size()-1; i++) 
    			{
    				for (int k = i+1; k < list.size(); k++) 
    				{			
    					if(list.get(i).equals(list.get(k)))
    					{
    						Assert.fail("Mismatch in data present in the list");
    					}				      
    				}	      
    			}		
    			return true;
    		} 
     
     //Verify No results page 
     public Hippo_LatinAmerica verifyNoResultsPage(String keyword){
    	try
    	{
    		getCommand().waitFor(5);
    	    LatamxSearchData searchData = LatamxSearchData.fetch("SearchData");
    		String searchDataFetched = searchData.keyword;
    		log("Search for keyword <B>"+searchDataFetched+"</B> from home page",LogType.STEP);
    		getCommand().isTargetVisible(HEADER_SEARCH_INPUT);
    		getCommand().clear(HEADER_SEARCH_INPUT);
    		getCommand().sendKeys(HEADER_SEARCH_INPUT, searchDataFetched);
    		getCommand().click(HEADER_SEARCH_BUTTON);
    		getCommand().waitFor(5);
    		
    		String noSearchResultsText = getCommand().getText(noSearchResults);
    		log("ssearch"+searchDataFetched,LogType.STEP);
    		log("Buscar resultados para "+""+searchDataFetched+"",LogType.STEP);
    		if(getCommand().isTargetVisible(noSearchResults) && noSearchResultsText.equalsIgnoreCase("Buscar resultados para "+"\""+searchDataFetched+"\"")) {
                log(noSearchResultsText+" is displayed",LogType.STEP);
    		}else {
                log(noSearchResultsText+" is not displayed",LogType.ERROR_MESSAGE);
                Assert.fail(noSearchResultsText+" is not displayed");
         }
    		
    	
    	 String didYouMeanFeatureText = getCommand().getText(didYouMeanFeature);
    	 
    	 String alternativeSearchOptionText = getCommand().getText(alternativeSearchOption);
    	 
    	 String browserName = caps.getBrowserName(); 
   	  
   	      if(browserName.equals("safari"))
            {
    	 if(getCommand().isTargetVisible(didYouMeanFeature) && didYouMeanFeatureText.equalsIgnoreCase("¿Quiso decir?"+alternativeSearchOptionText)) {
             log(didYouMeanFeatureText+" is displayed",LogType.STEP);
    		}
      
        else {
             log(didYouMeanFeatureText+" is not displayed",LogType.ERROR_MESSAGE);
             Assert.fail(didYouMeanFeatureText+" is not displayed");
           }
            }
    	 else
    	 { if(getCommand().isTargetVisible(didYouMeanFeature) && didYouMeanFeatureText.equalsIgnoreCase("�Quiso decir?"+alternativeSearchOptionText)) {
             log(didYouMeanFeatureText+" is displayed",LogType.STEP);
    		}
      
        else {
             log(didYouMeanFeatureText+" is not displayed",LogType.ERROR_MESSAGE);
             Assert.fail(didYouMeanFeatureText+" is not displayed");
           }
    	}
    	}
    	catch(Exception ex) {
            Assert.fail(ex.getMessage());
            }
    	 return this;
    	
    		}
    	 
     //Verify PDP displays when SKU# is entered
     public Hippo_LatinAmerica verifyPDPDisplays(String keyword) {
    	 
    	 try {
    		 
    		 	LatamxSearchData searchDataPDP = LatamxSearchData.fetch("SearchDataPDP");
    		 
    			String searchDataPDPFetched = searchDataPDP.keyword;
    			
    			log("Search for keyword <B>"+searchDataPDPFetched+"</B> from home page",LogType.STEP);
    			
    			getCommand().isTargetVisible(HEADER_SEARCH_INPUT);
    			
    			getCommand().clear(HEADER_SEARCH_INPUT);
    			
    			log("Sending the input in search box", LogType.STEP);
    			
    			getCommand().sendKeys(HEADER_SEARCH_INPUT, searchDataPDPFetched);
    			
    			log("Clicking on Serach button", LogType.STEP);
    			
    			getCommand().click(HEADER_SEARCH_BUTTON);
    			
    			getCommand().waitFor(9);
    			
    			
    			String productNumberText = getCommand().getText(productNumber);
    			
    			log("Fetching the current Url",LogType.STEP);
    			
    			String currentUrl = getCommand().getPageUrl();
    			
    			if(currentUrl.contains(productNumberText)) {
    				
    			 log("Product number is verified and Product PDP correctly displays", LogType.STEP);
    			 
    			}else {
    				
    			 log("Product number is not verified and Product PDP is not correctly displays", LogType.ERROR_MESSAGE);
    			 
    			 Assert.fail("Product number is not verified and Product PDP is not correctly displays");
    			 
    			}
    			
    	 }
    	 catch(Exception ex) {
    			
    	        Assert.fail(ex.getMessage());
    	        
    		}
    	 return this;
    		 
    	 }
    	  
     // Verify Language selector
     public Hippo_LatinAmerica verifylanguageSelector(){
    	 try {
    	
    		getCommand().click(ES_LINK);
    		getCommand().waitFor(5);
    		WebElement languageSelectorES =getCommand().driver.findElement(By.xpath("(//a[@class='koh-link-language-right koh-link-language-active'])[1]"));
    		String languageSelectorESClass = languageSelectorES.getAttribute("class");
		
    		String contentText = getCommand().getText(content);
    		
    		if(languageSelectorESClass.contains("active")) {
    			
    			log(contentText +" content is displayed in selected language when clicked on ES", LogType.STEP);
    			
    		}else {
    			
    			log(contentText +" content is displayed in other language when clicked on ES", LogType.ERROR_MESSAGE);
    			Assert.fail(contentText +" content is displayed in other language when clicked on ES");
    			
    		}
    		
    		getCommand().click(EN_LINK);
    		getCommand().waitFor(5);
    		WebElement languageSelectorEN = getCommand().getDriver().findElement(By.xpath("(//a[@class='koh-link-language-left koh-link-language-active'])[1]"));
    		
    		String languageSelectorENClass = languageSelectorEN.getAttribute("class");

    		String contentENText = getCommand().getText(contentEN);
    		
    		if(languageSelectorENClass.contains("active")) {
    			
    			log(contentENText +" content is displayed in selected language when clicked on EN", LogType.STEP);
    			
    		}else {
    			
    			log(contentENText +" content is displayed in other language when clicked on EN", LogType.ERROR_MESSAGE);
    			
    			Assert.fail("content is displayed in other language when clicked on EN");
    			
    	     	}
    		 }
    	 catch(Exception ex) {
    			
    	        Assert.fail(ex.getMessage());
    	        
    		}
    	
    	return this;
    	
     }

     //Verify global nav unfolding animation
     public Hippo_LatinAmerica verifyUnfoldingAnimation() {
    	 
    	 try {
    		 
    		 List<WebElement> Lists = getCommand().driver.findElements(By.xpath("//*[@id='koh-primary-nav-menu']/ul/li"));
    		 String pageTitle = getCommand().driver.getTitle();
    		 int i=1;
    		 log("Clicking on each global nav options",LogType.STEP);
    		 for(WebElement List : Lists)
 			 {
 				List.click();
 				getCommand().waitFor(6);
                ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
				
				if(listofTabs.size()<=1)
				{
					WebElement SubNav = getCommand().driver.findElement(By.xpath("//*[@id='koh-primary-nav-menu']/ul/li["+i+"]/div"));
					String ClassAfterOpening = SubNav.getAttribute("class");
					String StyleAfteropening = SubNav.getAttribute("style");
					
					if(SubNav.isDisplayed() && ClassAfterOpening.contains("open") && StyleAfteropening.contains("block")) {
						log("Clicking on "+i+" option opens SubMenu",LogType.STEP);
					}
					
					else {
						log("Clicking on "+i+" not unfolded & opening SubMenu",LogType.ERROR_MESSAGE);
						Assert.fail("Clicking on "+i+" option not unfolded & opening SubMenu");
					}
					log("Again Clicking on "+i+" option nav option",LogType.STEP);
					
					List.click();	
					
					getCommand().waitFor(3);
		    	      
	    	        String ClassAfterFolding= SubNav.getAttribute("class");
					String StyleAfterFolding = SubNav.getAttribute("style");
					
					if(!SubNav.isDisplayed() && !ClassAfterFolding.contains("open") && !StyleAfterFolding.contains("block")) {
						log("Clicking on expanded "+i+" option unfolds the SubMenu",LogType.STEP);
					}
					
					else {
						log("Clicking on expanded "+i+" nav option not unfolds the SubMenu",LogType.ERROR_MESSAGE);
						Assert.fail("Clicking on expanded "+i+" nav option not unfolds the SubMenu");
					}
					i=i+1;
				}
				else
				{
					log("Checking Click on Ideas opens new tab on inspiracionkohler.com",LogType.STEP);
					
					
					 if(browserName.equals("safari"))
						{

							getCommand().driver.switchTo().window(listofTabs.get(0));
						}
		    		 else
		    		 {
							getCommand().driver.switchTo().window(listofTabs.get(1));

		    		 }
					
								
					
					String CurrentpageTitle = getCommand().getPageTitle();
					String Currentpageurl = getCommand().getPageUrl();
					
					
					if(pageTitle.trim().equals(CurrentpageTitle) && !Currentpageurl.contains("inspiracionkohler.com"))
					{
						log("Clicking on Ideas not opening a new tab",LogType.ERROR_MESSAGE);
						Assert.fail("Clicking on Ideas not opening a new tab");
					}
					
					else
					{
						log("Clicking on Ideas, New tab opens on inspiracionkohler.com",LogType.STEP);
						
					}
 			 
    	 }
    	 }
     
    	 }
    	 catch(Exception ex) {
    			
    	        Assert.fail(ex.getMessage());
    	        
    		}
    	
    	return this;	
     }
      
     //Verify QuickView of a product
     public Hippo_LatinAmerica verifyQuickViewOfAProduct() {
    	 
    	try {
    		
    		getCommand().click(KITCHEN);
    		getCommand().waitFor(2);
    		
    	 	getCommand().click(KITCHENITEM);
    	 	getCommand().waitFor(5);
    		WebElement mouseHoverOnProduct = getCommand().getDriver().findElement(By.xpath("(//div[@class='koh-product-quick-view'])[2]"));
    		
    		Actions action = new Actions(getCommand().getDriver());
    		
    		action.moveToElement(mouseHoverOnProduct).build().perform();
    		
    		getCommand().click(QUICKVIEW);
    		    		
    		getCommand().isTargetAvailable(QUICKVIEWDISPLAY);
    		
    		log("QuickView of a product is displayed", LogType.STEP);
    		
    		getCommand().isTargetAvailable(PRODUCTIMAGE);
    		
    		log("Product Image is dispalyed", LogType.STEP);
    		
    		getCommand().isTargetAvailable(PRODUCTCONTENT);
    		
    		log("Product content is displayed", LogType.STEP);
    		
    		List<WebElement> colors = getCommand().getDriver().findElements(By.xpath("(//div[@class='koh-product-colors'])[2]/ul/li"));
    		
    		int colorsAvailable = colors.size();
    		 
    		log(colorsAvailable+" No of colors available for this product",LogType.STEP);

    		String productDescriptionText = getCommand().getText(productDescription);
    		
    		String actualProductDescriptionText = "instalación";
    		
    		if(productDescriptionText.contains(actualProductDescriptionText)) {
    			
    			log(productDescriptionText+ "Product description is verified", LogType.STEP);
    			
    		}else {
    			
    			log(productDescriptionText+ "Product description is not verified", LogType.ERROR_MESSAGE);
    			Assert.fail("Product description is not verified");
    			
    		}
    		getCommand().isTargetAvailable(COMPARECTA);
    		
    		log("COMPARECTA is displayed", LogType.STEP);
    		
    		}
    	catch(Exception ex) {
    		
            Assert.fail(ex.getMessage());
            
    	}
    	return this;
    	
    }

     //Verify Search Results page
     public Hippo_LatinAmerica verifySearchResultsPage(String Data) {
    	
    	try {
    		
    		LatamxSearchData searchWord = LatamxSearchData.fetch(Data);
    		
    		String searchWordFetched = searchWord.keyword;
    		
    		log("Search for keyword <B>"+searchWordFetched+"</B> from home page",LogType.STEP);
    		
    		log("Selecting the EN link",LogType.STEP);
    		 
    		 getCommand().click(EN_LINK); 
    		 getCommand().waitFor(3);
    		
    		getCommand().isTargetVisible(HEADER_SEARCH_INPUT);
    		
    		getCommand().clear(HEADER_SEARCH_INPUT);
    		
    		log("Sending the input in search box", LogType.STEP);
    		
    		getCommand().sendKeys(HEADER_SEARCH_INPUT, searchWordFetched);
    		
    		log("Clicking on Search button", LogType.STEP);
    		
    		getCommand().click(HEADER_SEARCH_BUTTON);
    		getCommand().waitFor(10);
    		
    		List<WebElement> searchResults = getCommand().getDriver().findElements(By.xpath("//div[@class='koh-search-controls']"));
    		
    		String searchResultsText;
    		
    		for(WebElement text : searchResults) {
    			
    			searchResultsText = text.getText();
    			
    			log(searchResultsText +" searchResultsText is displayed",LogType.STEP);
    		}
    	}
    	catch(Exception ex) {
    		
            Assert.fail(ex.getMessage());
            
    	}
    	return this;
    	
    }
    
     //Verify Category page templates
     public Hippo_LatinAmerica verifyCategoryPage() {
    	 
    	 try {
    		 log("Selecting the EN link",LogType.STEP);
    		 getCommand().waitFor(5);
    		 getCommand().click(EN_LINK); 
    		 getCommand().waitFor(3);
    		 getCommand().isTargetVisible(KITCHEN);
    		 getCommand().waitFor(3);
    		 getCommand().click(KITCHEN);
    		 getCommand().waitFor(3);
    		 getCommand().click(KITCHENITEM);
    		 getCommand().waitFor(5);
    		 String currentUrl = getCommand().getPageUrl();
    		 
    		 String exactCurrentUrl = currentUrl.replace("+", " ");
    		 
    		 String defaultCategoryText = getCommand().getText(defaultCategory);
    		 
    		 if(exactCurrentUrl.contains(defaultCategoryText)) {
    			 
    			 log(defaultCategoryText+ "Category is pre selected",LogType.STEP);
    			 
    		 }else {
    			 
    			 log(defaultCategoryText+"Category is not pre selected",LogType.ERROR_MESSAGE);
    			 Assert.fail(defaultCategoryText+"Category is not pre selected");
    			 
    		 }
    		 getCommand().isTargetVisible(SORTBY);
    	
    		 List<WebElement> productGrid = getCommand().getDriver().findElements(By.xpath("//div[@class='koh-search-results']/div"));

    		 int count = productGrid.size();
    		 
    		 log(count+" no of productGrid",LogType.STEP);
    		 
    		 if(count > 0) {
    			 
    			 log("Category page contains prodcutGrid",LogType.STEP);
    		 }
    		 else {
    			 
    			 log("Category page does not contains prodcutGrid",LogType.ERROR_MESSAGE);
    			 
    			 Assert.fail("Category page does not contains prodcutGrid");
    		 }
    	 }
    	 catch(Exception ex) {
    		 
             Assert.fail(ex.getMessage());
      }
    		 return this;
    	
     }

     //Method to verify SkuUpdates
     public Hippo_LatinAmerica verifySKUUpdate(String Data) {
    	 try
         {
                log("Sending product details in to search box control",LogType.STEP);
                
                LatamxSearchData searchWord = LatamxSearchData.fetch(Data);
                
                String Product = searchWord.keyword;
                log("Search for keyword <B>"+searchWord+"</B> from home page",LogType.STEP);
                
                getCommand().isTargetVisible(HEADER_SEARCH_INPUT);
                
                getCommand().sendKeys(HEADER_SEARCH_INPUT, Product);
                
                log("Clicking on search button",LogType.STEP);
                
                getCommand().click(HEADER_SEARCH_BUTTON);
                
                getCommand().waitFor(5);
          
                List<String> ProductSkus = new ArrayList<String>();
          
                log("Getting all the color chips available for the searched product and Clicking on each color chip",LogType.STEP);
                
                List<WebElement> Colors = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[2]/div/ul/li/span"));
                
                int i=1;
                
                for(WebElement Color : Colors) 
                {
                      log("Clicking on "+i+" color chip and getting the sku# for the selected color chip",LogType.STEP);
                      Color.click();
                      getCommand().waitFor(3);
                      String Sku = Color.getAttribute("class");
                      if(Sku.contains("koh-selected-variant")) 
                      {
                             log("Getting the SKU# for selected color chip and adding to list",LogType.STEP);
                             String ProductSku = getCommand().getText(ProductId_PDP);
                             
                             ProductSku = ProductSku.substring(2);
                             
                             ProductSkus.add(ProductSku);
                             i++;
                      }
                      
                      else 
                      {
                             log("Clicking on "+i+" color chip is not get selected",LogType.ERROR_MESSAGE);
                             Assert.fail("Clicking on "+i+" color chip is not get selected");
                      }
                }
                
                log("Checking Sku# is different for each selected color chip",LogType.STEP);
                
                getCommand().waitFor(3);                
                Assert.assertTrue(CompareDataFromSameList(ProductSkus),"Sku# is not getting updated for different color chip");
                
                log("Sku# is different for each selected color chip. Clicking on color chip SKU# updates",LogType.STEP);
         }
         
         catch(Exception ex) {
                Assert.fail(ex.getMessage());
         }

         return this;
    }
     
     //Verify sorting works properly and product grid in Category page
     public Hippo_LatinAmerica verifyProductCategorySortingFilter() {
    	 
    	 try
         {
                ArrayList<String> Defaultvalues= new ArrayList<String>();
                
                ArrayList<String> valuesAscOrder = new ArrayList<String>();
                
                ArrayList<String> valuesDscOrder = new ArrayList<String>();
                
                ArrayList<String> Relavancevalues = new ArrayList<String>();
                
                log("Navigating to product list page",LogType.STEP);
                getCommand().waitFor(5);
                
                getCommand().click(KITCHEN);
                getCommand().waitFor(5);
                
                               
                getCommand().isTargetPresent(KITCHENITEM);
                
                getCommand().click(KITCHENITEM);
                getCommand().waitFor(5);
                
                List<WebElement> ProductsBeforeSort = getCommand().driver.findElements(By.xpath("//div[@class='koh-search-results']/div/div/div/a/span[1]"));
                
                for(int l=0;l<ProductsBeforeSort.size();l++)
                {
                      String Text = ProductsBeforeSort.get(l).getText();                      
                      Defaultvalues.add(Text);  
                }

              if(getCommand().isTargetVisible(SORTBY))       	  
                {
        	  	      getCommand().click(SORTBY);
                      log("Sort drop down is Visible",LogType.STEP);

                      List<WebElement> SortOptions = getCommand().driver.findElements(By.xpath("//ul[@class='koh-sort-options open']/li"));
                      
                      for(int l=0 ; l<SortOptions.size();l++)
                      {
                             List<WebElement> Sortoptions = getCommand().driver.findElements(By.xpath("//ul[@class='koh-sort-options open']/li"));

                             String valueIs = Sortoptions.get(l).getText();
                             
                             if(valueIs.equals("Nombre Z-A")) 
                             {                                        
                                    Sortoptions.get(l).click();
                                    
                                    log("Selecting sort option "+valueIs,LogType.STEP);
                                    
                                   getCommand().waitFor(5);
                                    
                                    log("Getting all product name and adding to list after sorted with "+valueIs,LogType.STEP);
                                    
                                    List<WebElement> Product = getCommand().driver.findElements(By.xpath("//div[@class='koh-search-results']/div/div/div/a/span[1]"));
                                    
                                    for(int k=0;k<Product.size();k++)
                                    {
                                           String Text = Product.get(k).getText();                                        
                                           valuesDscOrder.add(Text);  
                                    }
                             }
                             
                             if(valueIs.equals("Nombre A-Z")) 
                             {      
                                    log("Selecting sort option "+valueIs,LogType.STEP);
                                    
                                    Sortoptions.get(l).click();

                                    getCommand().waitFor(5);
                                    
                                    log("Getting all product name and adding to list after sorted with "+valueIs,LogType.STEP);
                                    
                                    List<WebElement> Product = getCommand().driver.findElements(By.xpath("//div[@class='koh-search-results']/div/div/div/a/span[1]"));
                                    
                                    for(int k=0;k<Product.size();k++)
                                    {
                                           String Text = Product.get(k).getText();
                                           
                                           valuesAscOrder.add(Text);  
                                    }
                                    
                                    getCommand().mouseHover(SORTBY).click(SORTBY);
                             }
                             
                             if(valueIs.equals("Relevancia"))
                             {
                                   log("Selecting sort option "+valueIs,LogType.STEP);
                                    
                                    Sortoptions.get(l).click();

                                    getCommand().waitFor(5);
                                    
                                    log("Getting all product name and adding to list after sorted with "+valueIs,LogType.STEP);
                                    
                                    List<WebElement> Product = getCommand().driver.findElements(By.xpath("//div[@class='koh-search-results']/div/div/div/a/span[1]"));
                                    
                                    for(int k=0;k<Product.size();k++)
                                    {
                                           String Text = Product.get(k).getText();
                                           
                                           Relavancevalues.add(Text);  
                                    }
                                    
                                    getCommand().mouseHover(SORTBY).click(SORTBY);
                             }
                      }
                      
           ArrayList<String> actualValueIs = new ArrayList<String>();
                      
           actualValueIs.addAll(valuesAscOrder);
           
           Collections.sort(actualValueIs);
           
           log("Checking Sort values stored in list's",LogType.STEP);
           
           Assert.assertTrue(VerifySortByNameIs(valuesAscOrder,actualValueIs),"Sort is not working");
           
           Assert.assertTrue(VerifySortByNameIs(Defaultvalues,Relavancevalues),"Sort is not working");

           log("Sort is working",LogType.STEP);
                }
         }
         
         catch(Exception ex) {
                Assert.fail(ex.getMessage());
         }
    	return this;

     }
      
     //Verify that product are displayed in alphabetical order when sorted by NAME
     public static boolean VerifySortByNameIs(List<String> ExpectedVal ,List<String> ActualVal)
    			{
    				boolean status = false;
    				
    				if(ExpectedVal.size() ==  ActualVal.size())
    				{
    					if(ExpectedVal.equals(ActualVal))
    						status = true;
    				}
    								
    						
    				return status;
    			} 


}
		
		
	


	
	
